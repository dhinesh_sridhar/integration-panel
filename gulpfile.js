require("any-promise/register")("bluebird");

var gulp = require('gulp');
var jshint = require('gulp-jshint');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var inject = require('gulp-inject');
var mainBowerFiles = require('main-bower-files');
var modRewrite = require('connect-modrewrite');
var series = require('stream-series');
var clean = require('gulp-clean');
var useref = require('gulp-useref');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var gulpSequence = require('gulp-sequence');
var gulpUtil = require('gulp-util');
var templateCache = require('gulp-angular-templatecache');
var exists = require('path-exists').sync;
var ngConfig = require('gulp-ng-config');
var fs = require('fs');
var config = require('./config.js');
var debug = true; // (process.argv.indexOf("--debug") > -1) ? true : false;
var preSync = (debug == true) ? 'inject' : 'build';
var filter = require('gulp-filter');
var rev = require('gulp-rev');
var revReplace = require('gulp-rev-replace');
gulp.task('lint', function() {
  return gulp.src('./app/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});


gulp.task('cleanPublic', function() {
  return gulp.src(['dist/*'], { read: false })
    .pipe(clean());
});

gulp.task('copyPublic', function() {
  return gulp.src(['./app/**/*.png', './app/**/*.jpg', './app/**/*.jpeg', './app/**/*.gif', './app/**/ads.js', './app/lazy**/**', './bower_components/font-awesome/**/fontawesome-webfont.*', './bower_components/simple-line-icons/**/Simple-Line-Icons.*', './bower_components/bootstrap/**/glyphicons-halflings-regular.*','./app/*.ico'])
    .pipe(gulp.dest('./dist'));
});



gulp.task('inject', function() {
  var cssStream = gulp.src(['./app/**/*.css', '!./app/styles/custom.css'], { read: false });
  var customCssStream = gulp.src('./app/styles/custom.css', { read: false });

  var angularFilter = function(path) {
    if (path.indexOf("angular/angular.js") >= 0) {
      return false;
    }
    return true;
  }

  var minifiedPaths = function(files) {
    // Replace files by their minified version when possible
    var bowerWithMin = files.map(function(path, index, arr) {
      var newPath = path.replace(/.([^.]+)$/g, '.min.$1');
      return exists(newPath) ? newPath : path;
    });
    return bowerWithMin;
  }

  return gulp.src(['./app/index.html', './app/preview.html'])
    .pipe(inject(series(cssStream, customCssStream), { relative: true }))
    .pipe(inject(gulp.src(['./app/components/**/*.js'], { read: false }), { relative: true }))
    .pipe(inject(gulp.src(minifiedPaths(mainBowerFiles({ group: 'angularjs' })), { read: false }), { name: 'inject:bower:angular', relative: true }))
    .pipe(inject(gulp.src(minifiedPaths(mainBowerFiles({ group: 'editor', filter: angularFilter })), { read: false }), { name: 'inject:bower:editor', relative: true }))
    .pipe(inject(gulp.src(minifiedPaths(mainBowerFiles({ group: 'widgets', filter: angularFilter })), { read: false }), { name: 'inject:bower:widgets', relative: true }))
    .pipe(inject(gulp.src(minifiedPaths(mainBowerFiles({ group: '!exclude', filter: angularFilter })), { read: false }), { name: 'inject:bower:other', relative: true }))
    .pipe(gulp.dest('./app'))

});

gulp.task('html', function() {
  return gulp.src('./app/**/*.html')
    .pipe(templateCache({ module: 'partialTemplates', root: '/', standalone: true, filename: 'partial-templates.html.js' }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('concat', ['inject'], function() {
  var notMinified = function(file) {
    // TODO: add business logic
    console.log(file);
    //path, file.path.str.endsWith('min.js')
    return false;
  }

  var mangelCustom = gulpif("*.js", uglify().on('error', gulpUtil.log));
  var notMangleVendor = gulpif("**min.js", uglify({ mangle: false, compress: false }).on('error', gulpUtil.log), mangelCustom);
  var indexHtmlFilter = filter(['**/*', '!**/index.html'], { restore: true });
  return gulp.src('./app/index.html')
    .pipe(useref())
    .pipe(notMangleVendor)
    //.pipe(gulpif("*.js", notMinified))
    .pipe(gulpif('*.css', minifyCss()))
    .pipe(indexHtmlFilter)
    .pipe(rev())
    .pipe(indexHtmlFilter.restore)
    .pipe(revReplace())      
    .pipe(gulp.dest('./dist'));
});


gulp.task('clean', ['cleanPublic']);

gulp.task('update', ['concat', 'copyPublic']);

gulp.task('build',gulpSequence('ng-config','clean', 'html', 'update'));

gulp.task('browser-sync', [preSync], function() {
  browserSync.init({
    server: {
      baseDir: (debug == true) ? "./app" : "./dist",
      // The key is the url to match
      // The value is which folder to serve (relative to your current working directory)
      routes: {
        "/bower_components": "bower_components",
        "/node_modules": "node_modules"
      },
      middleware: [
        modRewrite([
          '!\\.\\w+$ /index.html [L]'
        ])
      ]
    },
    browser: "default"
  });
});

gulp.task('serve', ['ng-config','browser-sync'], function() {
  var watchDir = (debug == true) ? "./app/**/*.*" : "./dist/**/*.*";
  gulp.watch("./app/**/*.*", [preSync]);
  gulp.watch(watchDir).on('change', browserSync.reload);
});



/*
 *  We first generate the json file that gulp-ng-config uses as input.
 *  Then we source it into our gulp task.
 *  The env constants will be a saved as a sub-module of our app, ngEnVars.
 *  So we shall name it ngEnvVars.config.
 */
gulp.task('ng-config', function() {
  var ENV = process.env.PANEL_PUBLISHER_ENV || 'development';
  console.log(ENV, JSON.stringify(config[ENV]));
  fs.writeFileSync('./app-constant.json', JSON.stringify(config[ENV]));
  gulp.src('./app-constant.json')
    .pipe(
      ngConfig('devPanelApp', {
        createModule: false
      })
    )
    .pipe(gulp.dest('./app/'))
});

/*
 * To test beta env in local machine
 */
gulp.task('set-beta', function() {
  return ENV = process.env.PANEL_PUBLISHER_ENV = 'beta';
});

/*
 * To test production env in local machine
 */
gulp.task('set-prod', function() {
  return ENV = process.env.PANEL_PUBLISHER_ENV = 'production';
});
