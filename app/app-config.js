(function() {
  'use strict';

  angular.module('devPanelApp')
    .config(['$routeProvider', '$locationProvider', '$interpolateProvider', '$httpProvider', 'AnalyticsProvider', 'localStorageServiceProvider', 'LightboxProvider', 'tagsInputConfigProvider', 'toastrConfig', devPanelAppConfig]);
  angular.module('devPanelApp')
    .run(['$rootScope', '$q', '$location', '$window', 'Analytics', 'localStorageService', devPanelAppRun]);

  function devPanelAppConfig($routeProvider, $locationProvider, $interpolateProvider, $httpProvider, AnalyticsProvider, localStorageServiceProvider, LightboxProvider, tagsInputConfigProvider, toastrConfig) {

    var routes = [{
      route: '/account/:submenu',
      args: {
        templateUrl: '/components/accounts/access.html',
        auth: false
      }
    }, {
      route: '/account/:submenu/:key',
      args: {
        templateUrl: '/components/accounts/access.html',
        auth: false
      }
    }, {
      route: '/settings/:substep',
      args: {
        templateUrl: '/components/settings/settings.html',
        label: "Setting"
      }
    }, {
      route: '/:gameId/wizard/:step-:substep',
      args: {
        templateUrl: '/components/integrationwizard/integrationwizard.html',
        label: 'Wizard',
        reloadOnSearch: false
      }
    }, {
      route: '/:gameId/dau',
      args: {
        templateUrl: '/components/gameAnalytics/dau.html',
        label: 'Game Analytics',
        reloadOnSearch: false
      }
    }, {
      route: '/faq',
      args: {
        templateUrl: '/components/faq/faq.html',
        label: 'Faq',
        reloadOnSearch: false
      }
    }, {
      route: '/faq/answers',
      args: {
        templateUrl: '/components/faq/faqanswers.html',
        label: 'FaqAnswers'
      }
    }, {
      route: '/x/dashboard',
      args: {
        templateUrl: '/components/home/emptyhome.html',
        label: 'EmptyHome',
      }
    }, {
      route: '/:gameId/dashboard',
      args: {
        templateUrl: '/components/home/home.html',
        label: 'Home'
      }
    }, {
      route: '/:gameId/campaigns',
      args: {
        templateUrl: '/components/campaigns/directCampaigns.html',
        label: 'Campaign lists'
      }
    }, {
      route: '/:gameId/programmatic',
      args: {
        templateUrl: '/components/campaigns/progCampaigns.html',
        label: 'Prog Campaign lists'
      }
    }, {
      route: '/:anything*',
      args: {
        templateUrl: '/components/home/404home.html',
        label: 'ErrorHome',
      }
    }];

    angular.extend(toastrConfig, {
      newestOnTop: true,
      closeButton: false,
      target: 'body',
      templates: {
        toast: '/components/toaster/toaster.tmpl.html'
      },
      autoDismiss: true,
      setTimeout: 500,
      tapToDismiss: true,
    });

    LightboxProvider.getImageUrl = function(image) {
      return '/styles/assets/images/' + image.src;
    };
    tagsInputConfigProvider.setActiveInterpolation('tagsInput', { placeholder: true });


    var resolveFn = {
      'globalResolve': ['$location', '$rootScope', '$route', 'Global',
        function($location, $rootScope, $route, Global) {


          console.log('Global Resolve');
          $rootScope.globalLoading = true;

          function gotoLogin() {
            $rootScope.globalLoading = false;
            var currentLocation = $location.path();
            $location.path('/account/login').search({
              ref: currentLocation
            });
          };

          var globalPromise = Global.resolve({
            routeGameId: $route.current.params.gameId
          }).promise;

          globalPromise.then(function(data) {
            console.log(data);
            $rootScope.me = data.CurrentUser;
            $rootScope.currentGameProfile = data.CurrentGameProfile;
            $rootScope.currentGameId = data.CurrentGameId;
            $rootScope.currentGroup = data.CurrentUserGroup;
            $rootScope.currentRole = data.CurrentUserRole;
            $rootScope.gameProfiles = data.GameProfileList;
            $rootScope.globalFilter = {};
            $rootScope.dashboardFilter = {};

            $rootScope.globalLoading = false;
            $rootScope.globalError = null;

            var routeLabel = $route.current.$$route.label;
            if (routeLabel !== "Setting" && routeLabel !== "Faq" && routeLabel !== "FaqAnswers") {
              if (data.GameProfileList.count === 0) {
                $location.path("/new/wizard/1-a");
              } else if (routeLabel === 'EmptyHome') {
                $location.path("/" + data.GameProfileList.results[0].id + "/dashboard");
              }
            }
          }, function(error) {
            console.log('globalError', error);
            $rootScope.globalError = error.status;
            if (error.status == 401) {
              gotoLogin();
            } else {
              if (error.status !== -1) {
                $location.path("/error" + $location.path());
              }
            }
          });


          return globalPromise;
        }
      ]
    };

    routes.forEach(function(r) {
      if (r.args.auth !== false) {
        r.args.resolve = resolveFn;
      }
      delete r.args.auth;
      //console.log(r.route, r.args);
      $routeProvider.when(r.route, r.args);
    });

    $routeProvider
      .when('/', {
        redirectTo: '/x/dashboard'
      });

    $locationProvider.html5Mode(true);

    $httpProvider.interceptors.push('authInterceptor');
    AnalyticsProvider.setAccount({
      tracker: 'UA-72699183-1',
      trackEvent: true,
    });


    AnalyticsProvider.setDomainName('publisher.greedygame.com');
    AnalyticsProvider.trackUrlParams(true);
    AnalyticsProvider.ignoreFirstPageLoad(false);
    AnalyticsProvider.disableAnalytics(false);



    localStorageServiceProvider
      .setPrefix('gg');



  }

  function devPanelAppRun($rootScope, $q, $location, $window, Analytics, localStorageService) {
    $rootScope.isCollapsed = false;

    $rootScope.$on('$routeChangeSuccess', function(next, current) {
      Analytics.trackPage($location.path());
      Analytics.pageView();
    });

    $rootScope.$on('$routeChangeStart', function(event, next, current) {
      $rootScope.isLoginPage = next.params ? (next.params.submenu === 'login' || next.params.submenu === 'register' || next.params.submenu === 'retrieve-password' ? true : false) : false;
    });
  };
})();