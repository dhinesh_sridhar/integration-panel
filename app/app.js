'use strict';

angular
  .module('devPanelApp', ['partialTemplates','ngMessages', 'ngResource', 'ngRoute', 'ngCookies', 'angular-loading-bar', 'angular-google-analytics',
    'angular-intro', 'nvd3', 'ngFileUpload', 'ngTagsInput', 'ui.bootstrap', 'ngSanitize', 'ui.select',
    'timer', 'colorpicker.module', 'ngclipboard', 'LocalStorageModule',
    'ngAnimate', 'angularSpinner','bootstrapLightbox','daterangepicker','jkAngularRatingStars','toastr']);

