(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('Global', ['$q', '$filter', 'localStorageService', 'User', 'GameProfile', Global]);

  function Global($q, $filter, localStorageService, User, GameProfile) {
    var globalObj = {};

    globalObj.resolve = function(args) {
      //For Singleton approach
      console.log("Setting global promise.");
      globalObj._deferred = $q.defer();
      globalObj.promise = globalObj._deferred.promise;

      console.log("Resolving global service");

      var meDeferred = $q.defer();
      var gameDeferred = $q.defer();
      //Resolve me deferred
      console.log("localStorageService.get('token')", localStorageService.get('token'));
      if (!localStorageService.get('token')) {
        meDeferred.reject({ status: 401 });
      } else {
        User.me({}, function(data) {
          //Filter only pubisher groups
          console.log("The me data ", data);
          data.groups = $filter('filter')(data.groups, { type: 1 }, false);
          meDeferred.resolve(data);
        }, function(error) {
          meDeferred.reject(error);
        });
      }

      //Resolve game deferred, post me get resolved
      meDeferred.promise.then(function(me) {
        var currentGameId = args.routeGameId;
        if (currentGameId === 'new') {
          gameDeferred.resolve();
        } else if (currentGameId) {
          GameProfile.query({ id: currentGameId },
            function(data) {

              var emptyPlatform = [{ id: 1 }, { id: 2 }, { id: 3 }];
              data.platforms.forEach(function(nv) {
                emptyPlatform[nv.id - 1] = nv;
              });
              data.platforms = emptyPlatform;

              gameDeferred.resolve(data);
            },
            function(error) {
              gameDeferred.reject(error);
            });
        } else {
          gameDeferred.resolve();
        }
      });

      var allPromise = $q.all([meDeferred.promise, gameDeferred.promise]);
      allPromise.then(function(me_gameProfile) {
        console.log('allPromise Success: ', me_gameProfile);

        var me = me_gameProfile[0];
        var gameProfile = me_gameProfile[1];

        var currentGameId = gameProfile && gameProfile.id;
        var storeGroupId = localStorageService.get('groupId');
        var gameGroupId = (gameProfile != null ? gameProfile.group_owner.id : null);
        var zeroGroupId = (me.groups.length > 0 ? me.groups[0].id : null);
        var groupsLookup = [gameGroupId, storeGroupId, zeroGroupId];
        var groupId = null;
        var currentGroup = null;
        console.log('groupsLookup', groupsLookup);
        groupsLookup.forEach(function(gId) {
          if (gId && !groupId) {
            var filterGroup = $filter('filter')(me.groups, { id: parseInt(gId) }, true);
            console.log('filterGroup', filterGroup, parseInt(gId), gId);
            if (filterGroup.length > 0) {
              groupId = gId;
              currentGroup = filterGroup[0];
              return;
            }
          }
        });


        console.log('currentGroup', currentGroup);
        if (currentGroup && currentGroup.id) {

          if (storeGroupId != currentGroup.id) {
            localStorageService.set('groupId', currentGroup.id); //To be used in headers
          }
          GameProfile.query({}, function(data) {
            globalObj.GameProfileList = data;

            console.log(globalObj);

            globalObj._deferred.resolve(globalObj);

          });
        } else {
          alert("Currently setting up your account, please check back in some time")
          console.error("Currently setting up your account, please check back in some time");
        }

        globalObj.CurrentUser = me;
        globalObj.CurrentGameProfile = gameProfile;
        globalObj.CurrentGameId = gameProfile && gameProfile.id;
        globalObj.CurrentUserGroup = currentGroup;
        globalObj.CurrentUserRole = currentGroup.publisher_role;

      }, function(error) {
        console.error('allPromise Failed: ', error);
        if (error.status !== -1) {
          localStorageService.remove('token');
        }
        globalObj._deferred.reject({ error: true, status: error.status });

      });

      return globalObj;
    }

    return globalObj;

  };
})();