(function() {
  'use strict';
  angular.module('devPanelApp')
    .filter('datetime', datetime);
  angular.module('devPanelApp')
    .filter('lastSeenFilter', lastSeenFilter);
  angular.module('devPanelApp')
    .filter('unixEpochToDate', unixEpochToDate);
  angular.module('devPanelApp')
    .filter('enumConstantsFilter', ['enumConstants', enumConstantsFilter]);

  function enumConstantsFilter(enumConstants) {
    return function(enumData, type) {
      if (enumData === undefined) {
        return '';
      }
      return (enumConstants[type]) ? enumConstants[type][enumData] : '';
    };
  }

  function datetime() {
    return function(value, format) {
      if (!value) {
        return '';
      }
      var newDate = new Date(value);
      return newDate.toDateString();
    };
  };

  function lastSeenFilter() {
    return function(objects, item, minvalue) {

      var filteredItems = [];
      if (!objects || objects == []) {
        return filteredItems;
      }
      angular.forEach(objects, function(object) {

        if (object[item] > minvalue) {
          filteredItems.push(object);
        }
      });
      return filteredItems;
    }
  };

  function unixEpochToDate() {
    function getMonthName(m) {
      var str = '';
      switch (m) {
        case 0:
          str = 'Jan';
          break;
        case 1:
          str = 'Feb';
          break;
        case 2:
          str = 'Mar';
          break;
        case 3:
          str = 'Apr';
          break;
        case 4:
          str = 'May';
          break;
        case 5:
          str = 'Jun';
          break;
        case 6:
          str = 'Jul';
          break;
        case 7:
          str = 'Aug';
          break;
        case 8:
          str = 'Sep';
          break;
        case 9:
          str = 'Oct';
          break;
        case 10:
          str = 'Nov';
          break;
        case 11:
          str = 'Dec';
          break;
      }
      return str;
    };

    function formatDate(date) {
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0' + minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;

      return date.getDate() + '-' + getMonthName(date.getMonth()) + '-' + (date.getFullYear() % 100) + ' ' + strTime;
    };

    return function(value, format) {
      if (!value) {
        return '';
      }
      var newDate = new Date(value * 1000);
      return formatDate(newDate);
    };
  };
})();
