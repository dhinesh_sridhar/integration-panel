(function() {
  'use strict';

  angular.module('devPanelApp')
    .constant('enumConstants', {
      programaticBlacklistStatus: {
        1: 'Pending Approval',
        2: 'Approved',
        3: 'Pending Removal',
        4: 'Removed'
      },
      floorEcpmStatus: {
        1: 'Pending Approval',
        2: 'Approved',
        3: 'Pending Removal',
        4: 'Removed'
      },
      gameStatus: {
        0: 'Not Integrated',
        1: 'Testing',
        2: 'Live',
        3: 'Blacklisted'
      },
      apkTestingStatus: {
        1: 'Pending',
        2: 'QA Pass',
        3: 'QA Fail'
      },
      apkTestingType: {
        1: 'Error Manifest',
        2: 'Error Float',
        3: 'Error Native',
        4: 'Error Older GG Sdk',
        5: 'Error Others'
      }
    });
})();
