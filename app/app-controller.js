(function() {
  'use strict';
  angular.module('devPanelApp')
    .controller('MainCtrl', ['$scope', '$rootScope', '$route', '$routeParams', '$window', '$location', '$filter', '$uibModal',
      'Analytics', 'GameProfile', 'Intro', 'HelpIntro', 'User', 'Global', 'localStorageService', '$http', 'Notification', 'apiPrefix', MainCtrl
    ]);

  function MainCtrl($scope, $rootScope, $route, $routeParams, $window, $location, $filter,
    $uibModal, Analytics, GameProfile, Intro, HelpIntro, User, Global, localStorageService, $http, Notification, apiPrefix) {

    //$rootScope.mainaintenceMode = true;
    $rootScope.breadscrumbsList = [];
    $rootScope.actionableNotifications = [];
    $rootScope.$watch('me', function(nv, ov) {
      if (nv && nv.id) {
        Intercom('boot', {
          app_id: "ztvoyt3t",
          user_id: nv.id,
          name: nv.name, // Full name
          email: nv.email, // Email address
          created_at: (new Date(nv.date_joined)).getTime() / 1000 // Signup date as a Unix timestamp
        });
      }
    });

    $scope.$on('$routeChangeSuccess', function(next, current) {
      console.log(next, current);
      var routeLabel = current.$$route.label;
      $rootScope.isWizard = (routeLabel === 'Wizard');
      $rootScope.isSetting = (routeLabel === 'Setting');
      $rootScope.isDashboard = (routeLabel === 'Home');
      $rootScope.isThemeUnits = (routeLabel === 'Theme Units');
      $rootScope.faq = (routeLabel === 'Faq') || (routeLabel === 'FaqAnswers');
      $rootScope.isCollapsed = true;
      $scope.IntroOptions = ($rootScope.isWizard) ? Intro : HelpIntro($location.path());
      Intercom('update');
    });
    $scope.ChangeEvent = function(targetElement, scope) {

      console.log("Change Event called");
      console.log(targetElement); //The target element
      console.log(this); //The IntroJS object
    };

    $rootScope.$watch('currentGroup.id', function(nv, ov) {
      if (nv) {
        $http.get(apiPrefix + 'news/tasks').
        then(function success(data) {
          $rootScope.actionableNotifications = data.data;
        }, function error(error) {
          console.log(error);
        });
      }
    });
    
    $rootScope.getActionLink = function(actionableNotification) {
      if (actionableNotification.type == 'game') {
        var gameActionBaseUrl = '/' + actionableNotification.data.game_id + '/wizard/';
        if (actionableNotification.message.indexOf('Demographics') !== -1) {
          return gameActionBaseUrl + '1-a';
        }
        if (actionableNotification.message.indexOf('FloatUnit') !== -1) {
          return gameActionBaseUrl + '2-d';
        }
        if (actionableNotification.message.indexOf('NativeUnit') !== -1) {
          return gameActionBaseUrl + '2-b';
        }
      } else if (actionableNotification.type == 'payment') {
        return '/settings/payment';
      }
    }
    $rootScope.eventNotificationClick = function() {
      $http.post(apiPrefix + 'news/events');
    }

    $rootScope.setCurrentGroup = function(g) {
      console.log('setupGroupId', g.id);
      localStorageService.set('groupId', g.id);
      window.location.href = "/x/dashboard";
    };

    $rootScope.toggleCollapse = function() {
      $rootScope.isCollapsed = !$rootScope.isCollapsed;
    };

    $rootScope.clickTrack = function(category, action, lable) {
      if (typeof(label) == 'string') {
        lable = lable.toLowerCase();
      }
      Analytics.trackEvent(category, action, lable);
    }


    $rootScope.logout = function() {
      // Erase the token if the user fails to log in
      localStorageService.remove('token');
      //delete $rootScope.token;
      //$rootScope.me = null;
      $window.location.href = '/';
    };



    /*** Main Scope */
    $scope.isMenuActive = function(name) {
      var p = $location.path();
      var ret = (p.indexOf(name) >= 0);
      return ret;
    };

    $scope.reload = function() {
      $window.location.reload();
    }

    $scope.changeGraph = function(graph) {
      $scope.graphToShow = graph;

    };
    $scope.graphToShow = 'allGraph';

    $scope.isError = function(data) {
      return data !== null;
    };

    $scope.replaceGameInPath = function(gameId) {
      var path = $location.path();
      if ($routeParams.gameId === 'new' && path.indexOf('wizard') >= 0) {
        return path;
      } else if ($route.current.$$route) {

        var currentRoute = $route.current.$$route.originalPath;
        var params = $routeParams;
        params.gameId = gameId;
        angular.forEach(params, function(value, key) {
          currentRoute = currentRoute.replace(':' + key, value);
        });
        currentRoute = currentRoute.replace("?", "");
        return currentRoute;
      }
    };
  };
})();
