(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('authInterceptor', ['$rootScope', '$q', 'localStorageService', authInterceptor]);

  function authInterceptor($rootScope, $q, localStorageService) {
    $rootScope.ERROR_RESPONSES = [];
    return {
      request: function(config) {
        $rootScope.serverStatus = 0;
        config.headers = config.headers || {};
        var token = localStorageService.get('token');
        var groupId = localStorageService.get('groupId');
        if (!config.headers.noauth && token) {
          config.headers.Authorization = 'Token ' + token;
          config.headers.UserGroup = groupId;
        } else {
          delete config.headers.noauth;
        }
        return config;
      },
      response: function(response) {
        if (response.status === 401) {
          // handle the case where the user is not authenticated
        }

        return response || $q.when(response);
      },
      responseError: function(response) {
        console.log("the error response", response);
        $rootScope.serverStatus = response.status;
        if ((response.status >= 500)) {
          console.log("the 500 error");
          response.data = { "non_field_errors": ["We have encounter a server error!"] };
        }
        if (response.status == -1) {
          console.log("Internet is not connected");
          response.data = { "non_field_errors": ["Please check your Internet connection"] };
        }
        response.datetime = (new Date).getTime();
        $rootScope.ERROR_RESPONSES.push(response);
        console.error(response);
        return $q.reject(response);
      }
    };
  };
  //for capturing err
  angular.module('devPanelApp').factory('$exceptionHandler', ['$window', '$log',
    function($window, $log) {
      if ($window.Raven) {
        console.log('Using the RavenJS exception handler.');
        return function(exception, cause) {
          $log.error.apply($log);
          Raven.captureException(exception);
        };
      } else {
        console.log('Using the default logging exception handler.');
        return function(exception, cause) {
          $log.error.apply($log);
        };
      }
    }
  ]);
})();
