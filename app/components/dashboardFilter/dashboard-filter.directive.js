angular.module('devPanelApp')
  .directive('dashboardFilter', ['$q', '$filter', '$rootScope', 'Countries',
    function($q, $filter, $rootScope, Countries) {
      return {
        restrict: 'A',
        replace: true,
        scope: {
          currentGameProfile: '=',
          onChange: '&onChange'
        },
        templateUrl: '/components/dashboardFilter/dashboard-filter.html',
        link: function(scope, element, attrs, controller) {
          scope.date = {
            startDate: moment().subtract(7, "days"),
            endDate: moment()
          };
          scope.isTagChanged = false;
          scope.options = {
            applyClass: 'btn-green',
            autoApply: true,
            locale: {
              applyLabel: "Apply",
              fromLabel: "From",
              toLabel: "To",
              cancelLabel: 'Cancel',
              customRangeLabel: 'Custom range'
            },
            ranges: {
              'Today': [moment(), moment()],
              'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
              'Last 2 Days': [moment().subtract(1, 'days'), moment()],
              'Last 7 Days': [moment().subtract(6, 'days'), moment()],
              'Last 30 Days': [moment().subtract(29, 'days'), moment()]
            },
            eventHandlers: { 'apply.daterangepicker': _dateChange }
          };
          scope.filteredTags = [];
          scope.gameList = $rootScope.gameProfiles.results;
          scope.campaignTypelist = [{ id: 'all', value: 'All Campaigns' }, { id: 'direct', value: 'Direct Campaigns' }, { id: 'prog', value: 'Programmatic Campaigns' }];
          scope.countryList = Countries.localQuery();
          var allCampaignType = scope.campaignTypelist[0];
          allCampaignType.type = 'campaigntype'
          scope.filteredTags.push(allCampaignType);


          scope.$watch('currentGameProfile', function(nv) {
            var addedGame = $filter('filter')(scope.gameList, { id: nv.id })
            scope.gameList[scope.gameList.indexOf(addedGame[0])].isAdded = true;
            scope.gameList[scope.gameList.indexOf(addedGame[0])].type = 'game';
            scope.filteredTags.push(scope.gameList[scope.gameList.indexOf(addedGame[0])]);
            scope.searchFilter();
          });

          scope.addTag = function(type, item) {
            scope.isTagChanged = true;
            item.type = type;
            item.isAdded = true;
            if (type === 'campaigntype') {
              var campaignType = $filter('filter')(scope.filteredTags, { type: 'campaigntype' });
              if (campaignType.length > 0) {

                scope.filteredTags[scope.filteredTags.indexOf(campaignType[0])] = item;

                console.log(scope.filteredTags);
              } else {
                scope.filteredTags.push(item);
              }
            } else {

              scope.filteredTags.push(item);
            }

          }



          scope.searchFilter = function() {
            console.log("inside search filter", scope.date.startDate.clone().toDate(), scope.date.endDate.clone().toDate());
            if (scope.date.startDate && scope.date.endDate) {
              console.log(scope);
              var countryTags = $filter('filter')(scope.filteredTags, { type: 'country' });
              var gameTags = $filter('filter')(scope.filteredTags, { type: 'game' });
              var campaigntype = $filter('filter')(scope.filteredTags, { type: 'campaigntype' });
              var allFilter = {
                from_date: dateToEpoch(scope.date.startDate.clone().toDate()),
                till_date: dateToEpoch(scope.date.endDate.clone().toDate()),
                country: getCsvIds(countryTags),
                game: getCsvIds(gameTags),
                campaigntype: (campaigntype[0]) ? campaigntype[0]['id'] : 'all'
              };

              Object.keys(allFilter).forEach(function(key) {
                if (typeof allFilter[key] === 'undefined') {
                  delete allFilter[key];
                }
              });

              console.log(allFilter);
              scope.allFilter = allFilter;

              if (scope.onChange) {
                scope.onChange({ globalFilter: allFilter });
                scope.isTagChanged = false;
              }
            }
          }

          scope.getCampaigType = function(currentCampaign) {
            if (currentCampaign === 'direct') {
              return 'Branded Campaign'
            } else if (currentCampaign === 'prog') {
              return 'Programmatic Campaign'
            } else {
              return 'All Campaign'
            }
          }
          scope.loadTags = function(query, type) {
            console.log('query', query, type);
            var deferred = $q.defer();
            if (type === 'country') {
              var found = Countries.localQuery({ name: query });
              deferred.resolve(found);
            } else if (type === 'game') {

              var foundTitle = $filter('filter')($rootScope.gameProfiles.results, { title: query }, false);
              var foundId = $filter('filter')($rootScope.gameProfiles.results, { id: query }, false);
              var foundAll = angular.extend(foundTitle, foundId);
              deferred.resolve(foundAll);
            }
            return deferred.promise;
          }

          function getCsvIds(arrayObj) {
            if ((arrayObj) && (arrayObj.length !== 0)) {
              return (arrayObj || []).map(function(elem) {
                return elem.id;
              }).join(",");
            }
            return undefined;
          };

          function dateToEpoch(dateObj) {
            if (dateObj) {
              return parseInt((dateObj.getTime() / 1000) - (dateObj.getTimezoneOffset() * 60));
            }
            return undefined;
          }

          function _dateChange() {
            console.log("inside date change");
            scope.isTagChanged = true;
          }
        }
      };
    }
  ]);