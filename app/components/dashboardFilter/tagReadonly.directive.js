(function() {
  'use strict';

  angular.module('devPanelApp')
    .directive('tagReadonly', tagReadonly);

  function tagReadonly() {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.find('input').attr('onfocus', 'this.blur()');
        element.find('input').attr('style', 'cursor:default');
        element.find('input').attr('readonly','true');
      }
    };
  }
})();
