(function() {
  'use strict';

  angular.module('devPanelApp')
    .controller('faqAnswerCtrl', ['$routeParams', '$anchorScroll', '$timeout', '$location', '$filter', 'Faq', faqAnswerCtrl]);

  function faqAnswerCtrl($routeParams, $anchorScroll, $timeout, $location, $filter, Faq) {
    var ctrl = this;
    ctrl.highlight = parseInt($location.hash());
    var faqTags = [{ tag: 'Ad-Units', query: 'ad-units' }, { tag: 'Integration', query: 'integration' }, { tag: 'SDK', query: 'sdk' }, { tag: 'PanelApp', query: 'panelapp' }, { tag: 'Our business model', query: 'our_business_model' }, { tag: 'Publisher dashboard', query: 'publisher_dashboard' }, { tag: 'Payments', query: 'payments' }, { tag: 'Cross promotion', query: 'cross_promotion' }, { tag: 'Use our technology', query: 'use_our_technology' }, { tag: 'Brands', query: 'brands' }];
    ctrl.search = search;

    ctrl.faqTag = $routeParams.tag && ($filter('filter')(faqTags, { query: $routeParams.tag }))[0].tag;
    console.log("The tag is ",ctrl.faqTag);
    ctrl.faqQuery = $routeParams.tag && ($filter('filter')(faqTags, { query: $routeParams.tag }))[0].query;

    ctrl.searchedParamQuestion = $routeParams.q;
    ctrl.loading = true;
    ctrl.searchedQuestion = angular.copy(ctrl.searchedParamQuestion);
    

    Faq.get({
        tags: ctrl.faqQuery,
        q: ctrl.searchedParamQuestion
      },
      function(data) {
        ctrl.faqQuestions = data.results;
        ctrl.loading = false;
        $timeout(function() {
          $anchorScroll();
        });
      },
      function(error) {
        ctrl.faqError = error;
        ctrl.loading = false;
      });

    function search(searchedQuestion, event) {
      if ((event === undefined) || (event.which === 13)) {
        $location.path("/faq/answers/").search({ q: searchedQuestion });
      }
    }
  }
})();
