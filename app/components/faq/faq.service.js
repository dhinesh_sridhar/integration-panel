(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('Faq', ['$resource', '$cacheFactory', 'apiPrefix', Faq]);

  function Faq($resource, $cacheFactory, apiPrefix) {
    var cacheService = $cacheFactory('Faq');
    return $resource(apiPrefix + 'faqs', {}, {
      get: {
        method: 'GET',
        params: {},
        cache: cacheService
      }
    });
  };
})();
