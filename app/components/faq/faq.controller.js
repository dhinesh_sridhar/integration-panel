(function() {
  'use strict';

  angular.module('devPanelApp')
    .controller('faqCtrl', ['Faq', '$location', faqCtrl]);

  function faqCtrl(Faq, $location) {
    var ctrl = this;

    ctrl.search = search;
    ctrl.setCureentCollapsed = setCureentCollapsed;
    ctrl.faqQuestions = {};
    ctrl.isLoading = true;
    ctrl.faqTags = [{ tag: 'Ad-Units', query: 'ad-units' },
      { tag: 'Integration', query: 'integration' },
      { tag: 'SDK', query: 'sdk' },
      { tag: 'PanelApp', query: 'panelapp' },
      { tag: 'Our business model', query: 'our_business_model' },
      { tag: 'Publisher dashboard', query: 'publisher_dashboard' },
      { tag: 'Payments', query: 'payments' },
      { tag: 'Cross promotion', query: 'cross_promotion' },
      { tag: 'Use our technology', query: 'use_our_technology' },
      { tag: 'Brands', query: 'brands' }
    ];


    function search(searchedQuestion, event) {
      if ((event === undefined) || (event.which === 13)) {
        $location.path("/faq/answers/").search({ q: searchedQuestion });
      }
    }

    function setCureentCollapsed(faqTag) {
      if (faqTag.isCollapsed !== undefined) {
        faqTag.isCollapsed = !faqTag.isCollapsed;
      } else {
        faqTag.isCollapsed = true;
      }
    }
    for (var i = 0; i < ctrl.faqTags.length; i++) {
      ctrl.faqQuestions[ctrl.faqTags[i].query] = Faq.get({ tags: ctrl.faqTags[i].query },
        function(data){

        },function(error){
          ctrl.faqError = error;
      });
    }
    ctrl.isLoading = false;
  }
})();
