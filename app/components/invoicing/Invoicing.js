(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('Invoicing', ['$resource', 'apiPrefix', Invoicing]);

  function Invoicing($resource, apiPrefix) {
    var scheduler = $resource(apiPrefix + 'invoices/:element/:id', {
      id: '@id'
    }, {
      getInvoicesList: {
        method: 'GET',
        params: {
          element: 'list'
        }
      },
      getInvoiceUrl: {
        method: 'GET',
        params: {
          element: 'list',
        }
      },
      getInvoiceLink: {
        method: 'GET',
        params: {
          element: 'link'
        }
      },
      approveInvoice: {
        method: 'PUT',
        params: {
          element: 'update',
          status: 2,
          id: '@id'
        }
      }
    });
    return scheduler;
  };
})();