(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('Notification', ['$resource', '$cacheFactory', 'apiPrefix', Notification]);

  function Notification($resource, $cacheFactory, apiPrefix) {
    var cacheService = $cacheFactory('Notification');
    return $resource(apiPrefix + 'news/:type', {}, {
      get: {
        method: 'GET',
        cache: cacheService
      }
    });
  };
})();
