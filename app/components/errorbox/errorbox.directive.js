(function() {
  'use strict';
  angular.module('devPanelApp')
    .directive('errorbox', ['$window', errorbox]);

  function errorbox($window) {
    return {
      restrict: 'A',
      scope: {
        model: '=?ngModel',
        errorMessage: '@errorMessage'
      },
      templateUrl: '/components/errorbox/errorbox.html',
      link: function(scope, element, attrs) {

        console.log('errorMessage',scope.errorMessage);
        scope.isOnline = navigator.onLine;
        $window.addEventListener("offline", function() {
          scope.$apply(function() {
            scope.isOnline = false;
          });
        }, false);

        $window.addEventListener("online", function() {
          scope.$apply(function() {
            scope.isOnline = true;
          });
        }, false);

        scope.now = function() {
          return (new Date).getTime();
        };

        scope.dismissError = function(e) {
          var index = scope.model.indexOf(e);
          scope.model.splice(index, 1);
        };
        scope.removeError = function() {
          scope.model = '';
        }

        scope.removeMessage = function() {
          scope.errorMessage = '';
        }

        scope.formatErrorValue = function(v) {
          if (typeof v === 'string') {
            return v;
          } else if (Array.isArray(v)) {
            return v.join(',');
          }
        };
      }
    };
  };
})();
