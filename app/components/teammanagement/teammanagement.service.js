(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('Member', ['$resource', 'apiPrefix', Member]);

  function Member($resource, apiPrefix) {
    return $resource(apiPrefix + 'users/members/:id', {
      id: '@id'
    });
  };
})();
