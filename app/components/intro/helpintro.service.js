(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('HelpIntro', HelpIntro);

  function HelpIntro() {
    var IntroOptions = {
      showStepNumbers: false,
      showBullets: false,
      exitOnOverlayClick: true,
      exitOnEsc: true,
      nextLabel: '<strong>NEXT!</strong>',
      prevLabel: '<span style="color:green">Previous</span>',
      skipLabel: 'Exit',
      doneLabel: 'Done',
      steps: []

    };
    var getIntro = function(path) {
      if (path.indexOf('dashboard') > -1) {
        IntroOptions.steps = [{
          element: '#help-section-game',
          intro: "Add the game/games here to see relevant campaign details for the added game/games.By adding all games you can track the performance of your entire inventory.",
        }, {
          element: '#help-section-country',
          intro: "Add the countries for which you want to see the data",
        }, {
          element: '#help-section-date',
          intro: "By default this tab selects last 7 days.You can also choose",
        }, {
          element: '#help-section-campaigntype',
          intro: "Shows revenue from both Programmatic and Direct campaigns",
        }, {
          element: '#help-section-graphtype',
          intro: "Toggle between ‘Data Graph’ and ‘World Map’ view.Track metrics like ‘Revenue’,’Branded sessions’,’eCPS’ and ‘CTR",
        }, {
          element: '#help-section-graphdata',
          intro: "Toggle between ‘Data Graph’ and ‘World Map’ view.Track metrics like ‘Revenue’,’Branded sessions’,’eCPS’ and ‘CTR",
        }, {
          element: '#help-section-tabledata',
          intro: "Toggle between Game-wise,Country-wise and Date-wise data.",
        }];
      } else if (path.indexOf('campaigns') > -1) {
        IntroOptions.steps = [{
          element: '#help-section-campaignmonth',
          intro: "See all time campaign summaries or month wise campaign summaries.",
        }, {
          element: '#help-section-campaignmonthdownload',
          intro: "Easily download the report in .Excel or .csv",
        }, {
          element: '#help-section-campaigndate',
          intro: "See the daily valid sessions and the theme-wise session distribution",
        }, {
          element: '#help-section-campaigndatedownload',
          intro: "Easily download the report in .Excel or .csv",
        }];

      } else if (path.indexOf('programmatic') > -1) {
        IntroOptions.steps = [{
          element: '#help-section-progcampaigndate',
          intro: "See all time campaign summaries or month wise campaign summaries.",
        }, {
          element: '#help-section-progcampaigndownload',
          intro: "Easily download the report in .Excel or .csv",
        }, {
          element: '#help-section-progcampaignsort',
          intro: "See the daily valid sessions and the theme-wise session distribution",
        }];
      } else if (path.indexOf('team') > -1) {
        IntroOptions.steps = [{
          element: '#help-section-memberemail',
          intro: "Add the email address of the user",
        }, {
          element: '#help-section-membererole',
          intro: "select the role of the user, you have 3 options - Admin,Developer and Tester",
        }, {
          element: '#help-section-addmemberer',
          intro: "Finally click on ‘Add Member’ to successfully add the user",
        }];

      } else if (path.indexOf('reporting') > -1) {
        IntroOptions.steps = [{
          element: '#help-section-reportingfrequency',
          intro: "This section shows the frequency of the report you want to receive.",
        }, {
          element: '#help-section-reportingrecivers',
          intro: "Shows the email addresses of the recipients already added",
        }, {
          element: '#help-section-reportingaddrecivers',
          intro: "Enter the email address of the people you want to receive the report.",
        }, {
          element: '#help-section-reports',
          intro: "Shows the previously emailed reports."
        }];

      } else {
        IntroOptions.steps = [];
      }
      return IntroOptions;
    }

    return getIntro;
  };
})();
