(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('Intro', Intro);

  function Intro() {
    var IntroOptions = {
      steps: [{
        element: '#sidebarStep1',
        intro: "Please Select Any one Game",
      }, {
        element: '#integrationStep1',
        intro: "Please first save this GameProfile",
        position: 'right'
      }, {
        element: '#integrationStep2',
        intro: "Please upload atleast one Native and Float units",
      }],
      showButtons: false,
      showBullets: false,
      showProgress: false,
      showStepNumbers: false,
      exitOnOverlayClick: true,
      exitOnEsc: true,

    };

    return IntroOptions;
  };
})();
