(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('GameProfile', ['$resource', '$cacheFactory', 'apiPrefix', GameProfile]);

  function GameProfile($resource, $cacheFactory, apiPrefix) {
    var cacheService = $cacheFactory('GameProfile');
    var interceptor = {
      response: function(response) {
        cacheService.put(response.config.url, response.data);
        return response.data || $q.when(response);
      }
    };
    var resource = $resource(apiPrefix + 'games/:id/:entity/:entityid', {
      id: '@id'
    }, {
      get: {
        method: 'GET',
        params: {},
        cache: cacheService
      },
      query: {
        method: 'GET',
        params: {},
        cache: cacheService
      },
      dummy: {
        method: 'GET',
        params: {
          entity: 'dummy'
        }
      },
      screenshots: {
        method: 'GET',
        params: {
          entity: 'screenshots'
        },
        isArray: true
      },
      apk: {
        method: 'GET',
        params: {
          entity: 'apk'
        }
      },
      updateApkUrl: {
        method: 'POST',
        params: {
          entity: 'apk'
        }
      },
      save: {
        method: 'POST',
        params: {},
        interceptor: interceptor
      },
      trends: {
        method: 'GET',
        params: {
          entity: 'trends'
        },
        ignoreLoadingBar: true
      },
      themes: {
        method: 'GET',
        params: {
          entity: 'themes/trends'
        },
        ignoreLoadingBar: true
      },
      playstoreinfo: {
        method: 'GET',
        params: {
          entity: 'playstore-details'
        }
      },
      getGenresList: {
        method: 'GET',
        params: {
          entity: 'genres'
        }
      },
      addGenre: {
        method: 'POST',
        params: {
          entity: 'genres'
        }
      },
      getPlayStoreInfo: {
        method: 'GET',
        params: {
          entity: 'playstore-data'
        },
        ignoreLoadingBar: true
      }
    });

    resource.clearCache = function() {
      cacheService.removeAll();
    };

    return resource;
  };
})();
