(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('GameSettings', ['$resource', '$cacheFactory', 'apiPrefix', GameSettings]);

  function GameSettings($resource, $cacheFactory, apiPrefix) {
    var cacheService = $cacheFactory('GameSettings');

    var resource = $resource(apiPrefix + 'inventory/:settingType', {}, {
      getBlacklist: {
        method: 'GET',
        params: { settingType: 'blacklist' },
        cache: cacheService
      },
      addBlacklist: {
        method: 'POST',
        params: { settingType: 'blacklist' },
        cache: cacheService
      },
      removeBlacklist: {
        method: 'PUT',
        params: { settingType: 'blacklist' },
        cache: cacheService
      },
      getFloorecpmList: {
        method: 'GET',
        params: { settingType: 'ecpm' },
        cache: cacheService
      },
      addFloorecpm: {
        method: 'POST',
        params: { settingType: 'ecpm' },
        cache: cacheService
      },
      updateFloorecpmList: {
        method: 'PUT',
        params: { settingType: 'ecpm' },
        cache: cacheService
      },
      removeFloorecpmList: {
        method: 'PUT',
        params: { settingType: 'ecpm' },
        cache: cacheService
      }
    });
    resource.clearCache = function() {
      cacheService.removeAll();
    };
    return resource;
  };
})();
