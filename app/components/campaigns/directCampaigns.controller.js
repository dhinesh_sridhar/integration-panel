(function() {
  'use strict';

  angular.module('devPanelApp')
    .controller('CampaignListsCtrl', ['$rootScope', 'TableAnalytics', CampaignListsCtrl]);

  function CampaignListsCtrl($rootScope, TableAnalytics) {
    var ctrl = this;
    var currentDate = new Date();
    ctrl.campaign = {};
    ctrl.campaign.isLoading = false;
    ctrl.crossPromoted = {};
    ctrl.crossPromoted.isLoading = false;
    ctrl.campaign.fromDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
    ctrl.crossPromoted.fromDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);

    ctrl.getCampaign = getCampaign;
    ctrl.getCrossPromoted = getCrossPromoted;
    ctrl.crossPromotedDateChange = crossPromotedDateChange;
    ctrl.campaignDateChange = campaignDateChange;
    ctrl.exportCampaigns = exportCampaigns;
    ctrl.exportCrossPromotion = exportCrossPromotion;
    ctrl.format = 'MMMM-yyyy';
    ctrl.dateOptions = {
      dateDisabled: false,
      formatYear: 'yyyy',
      startingDay: 1,
      minMode: 'month'
    };
    ctrl.altInputFormats = ['M!/d!/yyyy'];


    function getCampaign(fromEpoch, tillEpoch) {
      if (fromEpoch && tillEpoch && $rootScope.currentGameId) {
        ctrl.campaign.isLoading = true;
        ctrl.campaignError =''
        TableAnalytics.getDirectCampaigns({
          game_id: $rootScope.currentGameId,
          from_date: fromEpoch,
          till_date: tillEpoch,
          cross_promoted: 0
        }, function(data) {
          ctrl.campaign.isLoading = false;
          ctrl.campaign.data = data;
        }, function(error) {
          ctrl.campaign.isLoading = false;
          ctrl.campaignError = error;
          console.log(error);
        });
      }
    };

    function getCrossPromoted(fromEpoch, tillEpoch) {
      if (fromEpoch && tillEpoch && $rootScope.currentGameId) {
        ctrl.crossPromoted.isLoading = true;
        ctrl.crossPromotedError =''
        TableAnalytics.getDirectCampaigns({
          game_id: $rootScope.currentGameId,
          from_date: fromEpoch,
          till_date: tillEpoch,
          cross_promoted: 1
        }, function(data) {
          ctrl.crossPromoted.isLoading = false;
          ctrl.crossPromoted.data = data;
        }, function(error) {
          ctrl.crossPromoted.isLoading = false;
          ctrl.crossPromotedError = error;
          console.log(error);
        });
      }
    }


    function campaignDateChange(newDate) {
      ctrl.campaign.fromDate = newDate;
      var fromDateEpoch = _getGMTEpoch(newDate);
      var tillDateEpoch = _getGMTEpoch(_getLastDayOfMonth(newDate.getMonth() + 1, newDate.getFullYear()));
      ctrl.getCampaign(fromDateEpoch, tillDateEpoch);
    };

    function crossPromotedDateChange(newDate) {
      ctrl.crossPromoted.fromDate = newDate;
      var fromDateEpoch = _getGMTEpoch(newDate);
      var tillDateEpoch = _getGMTEpoch(_getLastDayOfMonth(newDate.getMonth() + 1, newDate.getFullYear()));
      ctrl.getCrossPromoted(fromDateEpoch, tillDateEpoch);
    }

    //todo: need to create provider and callback for games load callback
    $rootScope.$watch('currentGameId', function(nv) {
      console.log('currentGameId ', nv);
      if (nv) {
        campaignDateChange(ctrl.campaign.fromDate);
        crossPromotedDateChange(ctrl.crossPromoted.fromDate);
      }
    });


    function exportCampaigns(type) {
      var extension = '.csv';
      if (type === 'excel') {
        extension = '.xls';
      }
      var date = ctrl.fromDate1;
      var filename = $rootScope.currentGameProfile.title + '_Campaign-Details_' + date + extension;
      //todo: need to make directive for it
      $('#campaign-details').tableExport({
        type: type,
        escape: 'false',
        tableName: filename
      });
    };

    function exportCrossPromotion(type) {
      var extension = '.csv';
      if (type === 'excel') {
        extension = '.xls';
      }
      var date = ctrl.fromDate1;
      var filename = $rootScope.currentGameProfile.title + '_Campaign-Details_' + date + extension;
      //todo: need to make directive for it
      $('#cross-promotion-details').tableExport({
        type: type,
        escape: 'false',
        tableName: filename
      });
    }

    function _getLastDayOfMonth(Month, Year) {
      return new Date((new Date(Year, Month, 1)) - 1);
    }

    function _getGMTEpoch(date) {
      return Math.floor((date.getTime() - (date.getTimezoneOffset() * 60 * 1000)) / 1000);
    }
  };
})();
