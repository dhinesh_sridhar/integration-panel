(function() {
  'use strict';

  angular.module('devPanelApp')
    .controller('ProgCampaignListsCtrl', ['$rootScope', 'TableAnalytics', ProgCampaignListsCtrl]);

  function ProgCampaignListsCtrl($rootScope, TableAnalytics) {

    var ctrl = this;
    var currentDate = new Date();
    ctrl.isTableLoading = false;

    ctrl.date = {
      startDate: moment().subtract(7, "days"),
      endDate: moment()
    };
    ctrl.options = {
      applyClass: 'btn-green',
      locale: {
        applyLabel: "Apply",
        fromLabel: "From",
        format: "D-MMMM-YY",
        toLabel: "To",
        cancelLabel: 'Cancel',
        customRangeLabel: 'Custom range'
      },
      opens: "left",
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 2 Days': [moment().subtract(1, 'days'), moment()],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()]
      },
      eventHandlers: { 'apply.daterangepicker': _dateChange }
    };
    ctrl.getCampaign = getCampaign;
    ctrl.calcTableTotal = calcTableTotal;





    function getCampaign(fromDateEpoch, tillDateEpoch) {
      ctrl.isTableLoading = true;
      ctrl.campaignsError ='';
      TableAnalytics.getCountryWise({
        game: $rootScope.currentGameId,
        from_date: fromDateEpoch,
        till_date: tillDateEpoch,
        campaigntype: 'prog'
      }, function(data) {
        ctrl.isTableLoading = false;
        ctrl.campaigns = data;
      }, function(error) {
        ctrl.campaignsError = error;
        ctrl.isTableLoading = false;
        console.log(error);
      })
    };

    ctrl.export = function(type) {
      var extension = '.csv';
      if (type === 'excel') {
        extension = '.xls';
      }
      var startdate = new Date(0);
      startdate.setUTCSeconds(ctrl.fromDateEpoch);
      var enddate = new Date(0);
      enddate.setUTCSeconds(ctrl.toDateEpoch)

      var date = '( ' + startdate + ' to ' + enddate + ' )';
      var filename = $rootScope.currentGameProfile.title + 'Programmatic_Campaign-Details_' + date + extension;
      $('#programatic-campaign-details').tableExport({
        type: type,
        escape: 'false',
        tableName: filename
      });
    };

    $rootScope.$watch('currentGameId', function(nv) {
      if (nv) {
        _dateChange();
      }
    });

    function calcTableTotal(type) {
      var total = 0;
      var i = 0
      for (i = 0; i < ctrl.campaigns.results.length; i++) {

        total = total + ctrl.campaigns.results[i][type];

      }
      return total;
    }

    function _dateChange() {
      var fromDateEpoch = _dateToEpoch(ctrl.date.startDate.clone().toDate()); // convrting moment to date object
      var tillDateEpoch = _dateToEpoch(ctrl.date.endDate.clone().toDate());
      getCampaign(fromDateEpoch, tillDateEpoch);
    }

    function _dateToEpoch(dateObj) {
      if (dateObj) {
        return parseInt((dateObj.getTime() / 1000) - (dateObj.getTimezoneOffset() * 60));
      }
      return undefined;
    }
  };
})();
