(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('Device', ['$resource', 'apiPrefix', Device]);

  function Device($resource, apiPrefix) {
    return $resource(apiPrefix + 'users/devices/:id/:entity', {
      id: '@id',
      entity: '@entity'
    }, {
      query: {
        method: 'GET',
        params: {}
      },
      create: {
        method: 'POST',
        params: {}
      }
    });
  };
})();
