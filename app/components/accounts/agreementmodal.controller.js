(function() {
  'use strict';
  angular.module('devPanelApp')
    .controller('agreementModalCtrl', ['$uibModalInstance', agreementModalCtrl])

  function agreementModalCtrl($uibModalInstance) {
    var ctrl = this;
    ctrl.close = close;
    ctrl.agree = agree;
    
    function close() {
      $uibModalInstance.dismiss('cancel');
    };

    function agree() {
      $uibModalInstance.close({$agreement:true})
    }
  };
})();
