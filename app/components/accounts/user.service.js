(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('User', ['$resource', '$cacheFactory', 'apiPrefix', User]);

  function User($resource, $cacheFactory, apiPrefix) {

    var cacheService = $cacheFactory('cacheUser');
    var interceptor = {
      response: function(response) {
        cacheService.removeAll();
        return response.data || $q.when(response);
      }
    };

    return $resource(apiPrefix + 'users/:id/:entity', {
      id: '@id',
      entity: '@entity'
    }, {
      games: {
        method: 'GET',
        params: {
          entity: 'games'
        },
        isArray: false
      },
      campaigns: {
        method: 'GET',
        params: {
          entity: 'campaigns'
        },
        isArray: false
      },
      create: {
        method: 'POST',
        params: {}
      },
      login: {
        method: 'POST',
        params: {
          id: 'authenticate',
          entity: 'auth'
        }
      },
      activate: {
        method: 'GET',
        params: {
          id: 'activate'
        }
      },
      me: {
        method: 'GET',
        params: {
          id: 'me'
        },
        cache: cacheService
      },
      connect: {
        method: 'POST',
        params: {
          id: 'authenticate'
        }
      },
      forgot: {
        method: 'POST',
        params: {
          id: 'forgot-password'
        }
      },
      retrieve: {
        method: 'POST',
        params: {
          id: 'retrieve-password'
        }
      },
      savePaymentDetail: {
        method: 'POST',
        params: {
          entity: 'payment-detail'
        }
      },
      saveCompanyDetail: {
        method: 'PUT',
        params: {
          entity: 'company'
        }
      },
      getCompanyDetail: {
        method: 'GET',
        params: {
          entity: 'company'
        }
      },
      getPaymentDetail: {
        method: 'GET',
        params: {
          entity: 'payment-detail'
        },
        interceptor: interceptor
      },
      changePassword: {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        transformRequest: function(obj) {
          var str = [];
          for (var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          return str.join("&");
        },
        params: {
          id: 'change-password'
        }
      },
    });
  }
})();
