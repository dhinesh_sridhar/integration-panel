(function() {
  'use strict';

  angular.module('devPanelApp')
    .controller('AccessCtrl', ['$rootScope', '$routeParams', '$location', '$uibModal', 'User', 'localStorageService', AccessCtrl]);

  function AccessCtrl($rootScope, $routeParams, $location, $uibModal, User, localStorageService) {
    var ctrl = this;
    $rootScope.messageResponse = null;
    ctrl.user = {};

    switch ($routeParams.submenu) {
      case 'login':
        ctrl.submenu = 'login'
        break;
      case 'reset':
        ctrl.submenu = 'resetpassword'
        break;
      case 'retrieve-password':
        ctrl.submenu = 'retrievepassword'
        break;
      case 'register':
        ctrl.submenu = 'signup'
        break;
    }

    //Change password message for password reset 
    if ($location.$$search.from) {
      var searchObject = $location.search();
      var fromdata = searchObject.from;
      if (fromdata === 'cgpwd') {
        $rootScope.changePasswordText = "Password has been changed! Please login with new credentials.";
      }
      delete $location.$$search.from;
    }

    if ($routeParams.submenu == 'activate') {
      ctrl.activationMsg = "Activating your account please wait...";
      User.activate({ entity: $routeParams.key }, function(data) {
        localStorageService.set('token', data.token);
        $location.path("/x/dashboard");
      }, function(error) {
        console.error(error);
      });
    }

    ctrl.login = function(form, user) {
      ctrl.loginError = '';
      $rootScope.messageResponse = null;
      var postLogin = '/';
      if ($location.$$search.ref) {

        var searchObject = $location.search();
        postLogin = searchObject.ref;
        delete $location.$$search.ref;
      }
      ctrl.master = angular.copy(user);
      user.login_as = 'publisher';
      var resource = User.login({}, user, function(data) {
        localStorageService.set('token', data.token);
        $rootScope.me = data;
        $location.path(postLogin);
      }, function(error) {
        ctrl.master = {};
        ctrl.loginError = error;
        // Erase the token if the user fails to log in
        localStorageService.remove('token');
      });
      return resource.$promise;
    };

    ctrl.retrieve = function(form, user) {
      ctrl.retriveError = '';
      $rootScope.messageResponse = null;
      user.login_as = 'publisher';
      var resource = User.forgot(user, function(data) {
          $rootScope.messageResponse = data.message;
        },
        function(error) {
          ctrl.retriveError = error;
          // Erase the token if the user fails to log in
          localStorageService.remove('token');
        });
      return resource.$promise;
    };

    ctrl.changePassword = function(form, user) {
      ctrl.isChangePasswordError = '';
      $rootScope.messageResponse = null;
      user.entity = $routeParams.key;
      var resource = User.retrieve(user, function(data) {
          ctrl.user = null;
          $rootScope.messageResponse = "Password has been changed! Please login with new credentials.";
          $location.path('account/login').search({
            from: 'cgpwd'
          });
        },
        function(error) {
          ctrl.isChangePasswordError = error;
        });
      return resource.$promise;
    };

    ctrl.createAccount = function(form, user) {
      ctrl.signUpError = '';
      $rootScope.messageResponse = null;
      user.login_as = 'publisher';
      var d = new Date();
      user.group_name = user.group_name || 'unkown_publisher_' + d.getTime();
      var resource = User.create({}, user, function(response) {
        $rootScope.messageResponse = response.message;
      }, function(error) {
        ctrl.signUpError = error;
        // Erase the token if the user fails to log in
        localStorageService.remove('token');
        delete $rootScope.token;
      });
      return resource.$promise;
    };

    ctrl.slugify = function(s) {
      s = s.replace(/[\s]/g, '');
      return s.toLowerCase();
    }

    ctrl.showAgreement = function() {
      var modalInstance = $uibModal.open({
        animation: false,
        templateUrl: '/components/accounts/agreementmodal.html',
        controller: 'agreementModalCtrl',
        controllerAs: 'ctrl',
        size: 'lg',
        backdrop: 'static',
        keyboard: false,
      });
      modalInstance.result.then(function(data) {
        ctrl.user.terms_agree = data.$agreement;
      }, function(dismiss) {
        console.log(dismiss);
      });

    };

  };
})();