(function() {
  'use strict';
  angular.module('nvd3')
    .directive('drawing', ['$q', drawing]);

  function drawing($q) {
    return {
      restrict: 'A',
      scope: {
        realEstates: '=ngRealEstates',
        unit: '=ngUnit'
      },
      templateUrl: '/components/adunits/areamarker.html',
      link: function(scope, element, attrs) {

        var colors = ['3498db', 'c0392b', '27ae60', 'ffa800', '745ec5',
          '8e44ad', '8eb021', 'e74c3c', '2980b9', '4f2b4f',
          '16a085', 'e67e22', '79302a', 'ef717a', '5b48a2',
          '27ae60', '3a6f81', '5e345e', '345f41', '5e4534',
          'eed5b7', '00bfff', 'ffd700', '8b4513', 'd02090',
          '98fb98', 'ffdead', '696969', 'b03060', '9acd32'
        ];

        console.log(scope.unit);

        scope.PSDEnum = {
          'pass': 'pass through',
          'norm': 'normal',
          'diss': 'dissolve',
          'dark': 'darken',
          'mul': 'multiply',
          'idiv': 'color burn',
          'lbrn': 'linear burn',
          'dkCl': 'darker color',
          'lite': 'lighten',
          'scrn': 'screen',
          'div ': 'color dodge',
          'lddg': 'linear dodge',
          'lgCl': 'lighter color',
          'over': 'overlay',
          'sLit': 'soft light',
          'hLit': 'hard light',
          'vLit': 'vivid light',
          'lLit': 'linear light',
          'pLit': 'pin light',
          'hMix': 'hard mix',
          'diff': 'difference',
          'smud': 'exclusion',
          'fsub': 'subtract',
          'fdiv': 'divide',
          'hue': 'hue',
          'sat ': 'saturation',
          'colr': 'color',
          'lum ': 'luminosity'
        }

        scope.getBlendName = function(n) {
          return scope.PSDEnum[n];
        }

        scope.setFilter = function(adunit, v) {
          if (!adunit.filters) {
            adunit.filters = [];
          }
          adunit.filters.push({
            filter: v,
            filterValue: 0
          });
        };

        scope.removeFilter = function(filters, f) {
          var index = filters.indexOf(f);
          if (index > -1) {
            filters.splice(index, 1);
          }
        };

        scope.setType = function(v) {
          scope.currentEditable.estate_type = v;
        };

        var dd = $q.defer();
        var d3 = null;
        dd.resolve(window.d3);
        dd.promise.then(function(_d3) {
          d3 = _d3;
          init();
        });

        var
          svg = null,
          g;

        scope.getBtnStyle = function(index) {
          return {
            'background-color': '#' + colors[index]
          };
        };

        function componentToHex(c) {
          var hex = c.toString(16);
          return hex.length == 1 ? "0" + hex : hex;
        };

        scope.currentEditable = null;
        scope.currentEditableIndex = null;
        scope.editEstate = function(index) {
          scope.currentEditableIndex = index;
          scope.currentEditable = scope.realEstates[index];
        };

        function drawEstate() {
          if (scope.realEstates) {
            var ids = 0;
            svg.selectAll('g').remove();
            angular.forEach(scope.realEstates, function(v) {
              var s = v.sector;
              var points = [];
              var i = 0;
              while (i < s.length) {
                var x = s[i];
                var y = s[i + 1];
                points.push([x, y]);
                i = i + 2;
              }
              v.filters = v.filters || [];

              svg.select('g.drawPoly').remove();
              var g = svg
                .append('g')
                .attr('id', 'estate-' + ids);

              g.append('polygon')
                .attr('estate-id', ids)
                .attr('points', points)
                .style('fill', '#' + colors[ids])
                .style('fill-opacity', 0.8)
                .style('stroke', '#000')
                .style('stroke-width', 2);

              points.splice(0);
              ids++;
            });
          }
        }

        function init() {
          // behaviors
          svg = d3.select(element[0]).select('svg');

          scope.$watch('realEstates', function() {
            scope.currentEditable = null;
            scope.currentEditableIndex = null;
            if (scope.unit && scope.unit.creative) {
              drawEstate();
            }
          });

          scope.$watch('unit.creative', function() {
            console.log(scope.unit.placement_png);
            if (scope.unit && scope.unit.creative) {
              var img = new Image();
              img.src = scope.unit.creative;

              img.onload = function() {
                var width = img.width;
                var height = img.height;

                svg.selectAll('g').remove();

                svg
                  .attr('height', height)
                  .attr('width', width);

                var e = angular.element(element[0].querySelector('div .svg-wrapper'));
                e.css({
                  'width': width,
                  'height': height,
                  'background-image': 'url(\'' + (scope.unit.placement_png ? scope.unit.placement_png : scope.unit.creative) + '\')',
                  'background-repeat': 'no-repeat'
                });
              };
            }
          });
        }
      }
    };
  };
})();
