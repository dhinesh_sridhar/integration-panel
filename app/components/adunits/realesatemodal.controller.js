(function() {
  'use strict';
  angular.module('devPanelApp')
    .controller('realEsateModalCtrl', ['$uibModalInstance', 'gameId', 'adunitId', 'Unit', realEsateModalCtrl]);

  function realEsateModalCtrl($uibModalInstance, gameId, adunitId, Unit) {
    var ctrl = this;

    ctrl.adunit = {};
    ctrl.adunit.isBusy = true;
    ctrl.currentRealEstates = {};
    ctrl.editMode = false;
    
    Unit.get({
      gameId: gameId,
      unitId: adunitId,
      unit_type: 0
    }, function(data) {
      ctrl.adunit = data;
      _getRealEstate(ctrl.adunit.id);
    }, function(error) {
      console.log(error);
    })


    ctrl.gameId = gameId;
    ctrl.toggleEdit = toggleEdit;
    ctrl.realEstateSave = realEstateSave;
    ctrl.close = close;



    function close() {
      $uibModalInstance.dismiss('cancel');
    };

    function _getRealEstate(unitId) {  
      Unit.realEstate({
        gameId: gameId,
        unitId: unitId
      }, function(data) {
        ctrl.currentRealEstates = {};
        data.results.forEach(function(val, i) {
          var s = val.sector.split(',');
          for (var l = 0; l < s.length; l++) {
            s[l] = parseInt(s[l]);
          }
          val.sector = s;
          ctrl.currentRealEstates[i] = val;
        });
        ctrl.adunit.isBusy = false;
      });
    }

    function toggleEdit() {
      ctrl.editMode = !ctrl.editMode;
    };

    function realEstateSave(isExit) {
      var s = [];
      for (var i in ctrl.currentRealEstates) {
        var d = angular.copy(ctrl.currentRealEstates[i]);
        d.sector = d.sector.join(',');
        Object.keys(d).forEach(function(key) {
          if (!d[key]) {
            delete d[key];
          }
        });
        s.push(d);
      }

      ctrl.adunit.isBusy = true;
      var unitId = ctrl.adunit.id;
      Unit.realEstateCreate({
        gameId: gameId,
        unitId: unitId
      }, s, function(data) {
        ctrl.currentRealEstates = {};
        data.results.forEach(function(val, i) {
          var s = val.sector.split(',');
          for (var l = 0; l < s.length; l++) {
            s[l] = parseInt(s[l]);
          }
          val.sector = s;
          ctrl.currentRealEstates[i] = val;
        });

        ctrl.adunit.isBusy = false;
        if (isExit) {
          $uibModalInstance.close(ctrl.adunit);
        }

      }, function(error) {
        ctrl.adUnitError = error;
        ctrl.adunit.isBusy = false;
      });
    };

  };
})();
