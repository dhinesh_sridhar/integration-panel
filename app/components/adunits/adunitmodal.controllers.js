(function() {
  'use strict';
  angular.module('devPanelApp')
    .controller('adUnitModalCtrl', ['$uibModalInstance', 'adunit', adUnitModalCtrl]);

  function adUnitModalCtrl($uibModalInstance, adunit) {
    var ctrl = this;
    ctrl.adunit = angular.copy(adunit);
    ctrl.close = _close;
    ctrl.saveUnit = _saveUnit;

    function _close() {
      $uibModalInstance.dismiss('cancel');
    };

    function _saveUnit() {
      $uibModalInstance.close(ctrl.adunit);
    };
  };
})();
