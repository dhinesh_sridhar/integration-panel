(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('Unit', ['$resource', '$cacheFactory', 'apiPrefix', Unit]);

  function Unit($resource, $cacheFactory, apiPrefix) {

    var cacheService = $cacheFactory('unitCache');
    var interceptor = {
      response: function(response) {
        cacheService.removeAll();
        return response.data || $q.when(response);
      }
    };


    var Unit = $resource(apiPrefix + ':gameId/units/:unitId/:entity', {
      gameId: '@gameId',
      unitId: '@id'
    }, {
      get: {
        method: 'GET',
        params: {},
        cache: cacheService
      },
      query: {
        method: 'GET',
        params: {},
        cache: cacheService
      },
      createFloatAd: {
        method: 'POST',
        params: {},
        interceptor: interceptor
      },
      removeUnit: {
        method: 'DELETE',
        interceptor: interceptor
      },
      upload: {
        method: 'POST',
        headers: {
          enctype: 'multipart/form-data'
        },
        interceptor: interceptor
      },
      realEstate: {
        method: 'GET',
        params: {
          entity: 'realestate'
        },
        cache: cacheService,
        interceptor: interceptor
      },
      realEstateCreate: {
        method: 'POST',
        params: {
          entity: 'realestate'
        },
        interceptor: interceptor
      },
      realEstateRemove: {
        method: 'DELETE',
        params: {
          entity: 'realestate'
        },
        cache: cacheService,
        interceptor: interceptor
      },
      placementS3Policy: {
        method: 'GET',
        params: {
          entity: 'placement-s3-policy'
        }
      },
    });

    Unit.clearCache = function() {
      cacheService.removeAll();
    };

    return Unit;
  };
})();
