(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('GeoLocationSearch', ['$resource', '$cacheFactory', 'apiPrefix', GeoLocationSearch]);

  function GeoLocationSearch($resource, $cacheFactory, apiPrefix) {
    var cacheService = $cacheFactory('GeoLocationSearch');
    return $resource(apiPrefix + 'geo/:element1', {}, {
      getCountry: {
        method: 'GET',
        params: {
          element1: 'country'
        },
        cache: cacheService
      },
      getState: {
        method: 'GET',
        params: {
          element1: 'state'
        }
      },
      getCity: {
        method: 'GET',
        params: {
          element1: 'city'
        }
      }
    });
  };
})();
