(function() {
  'use strict';

  angular.module('devPanelApp')

    .controller('SettingCtrl', ['$rootScope', '$filter', '$routeParams', '$location', '$window', '$http', '$q','toastr', 'apiPrefix', 'User', 'Member', 'Invoicing', 'GameSettings', 'Countries', 'GeoLocationSearch', SettingCtrl]);

  function SettingCtrl($rootScope, $filter, $routeParams, $location, $window, $http, $q,toastr, apiPrefix, User, Member, Invoicing, GameSettings, Countries, GeoLocationSearch) {
    var ctrl = this;

    $rootScope.viewPage = 'settings';
    ctrl.tabToShow = 'profile';
    ctrl.paymentMode = 'Bank Transfer';
    ctrl.teamMember = null;
    ctrl.teamMembers = [];
    ctrl.isloading = true;
    ctrl.newTeamMember = {};
    ctrl.changePassword = {};
    ctrl.companyProfile = {};
    ctrl.saveCompanyProfile = saveCompanyProfile;
    ctrl.resetCompanyProfile = resetCompanyProfile;
    ctrl.getRoleName = getRoleName;
    ctrl.isOperationAllowed = isOperationAllowed;
    ctrl.addMember = addMember;
    ctrl.editMember = editMember;
    ctrl.deleteMember = deleteMember;
    ctrl.savePaymentProfile = savePaymentProfile;
    ctrl.saveBasicProfile = saveBasicProfile;
    ctrl.resetBasicProfile = resetBasicProfile;
    ctrl.resetPaymentProfile = resetPaymentProfile;
    ctrl.isChangeBasicProfile = isChangeBasicProfile;
    ctrl.isChangePaymentProfile = isChangePaymentProfile;
    ctrl.isCompanyProfileChanged = isCompanyProfileChanged;
    ctrl.changeUserPassword = changeUserPassword;
    ctrl.addProgramaticBlacklist = addProgramaticBlacklist;
    ctrl.removeProgramaticBlacklist = removeProgramaticBlacklist;
    ctrl.addFloorecpm = addFloorecpm;
    ctrl.removeFloorecpm = removeFloorecpm;
    ctrl.updateFloorecpm = updateFloorecpm;
    ctrl.downloadInvoice = downloadInvoice;
    ctrl.approveInvoice = approveInvoice;
    ctrl.getCountryList = getCountryList;
    ctrl.getStateList = getStateList;
    ctrl.getCityList = getCityList;


    ctrl.countryList = Countries.localQueryWithoutOthers();
    ctrl.campaignType = [{ name: 'Programmatic', id: 'programmatic' }, { name: 'Direct', id: 'direct' }];

    ctrl.tabToShow = $routeParams.substep;
    if (!ctrl.tabToShow) {
      $location.path("account/settings/profile");
    }
    ctrl.paymentModes = [{ name: 'Wire Transfer', id: 0 }, { name: 'Paypal', id: 1 }];
    GameSettings.getBlacklist({},
      function(data) {
        ctrl.programaticBlacklist = data;
        console.log(data);
      },
      function(error) {
        ctrl.programaticBlacklistError = error;
        console.log(error);
      });

    GameSettings.getFloorecpmList({},
      function(data) {
        ctrl.floorEcpms = data;
      },
      function(error) {
        ctrl.floorEcpmsError = error;
        console.log(error);
      });
    User.getCompanyDetail({},
      function(data) {
        console.log("inside company detail", data);
        //getting all country data
        getCountryList('', data.country);
        if (data.country && data.country.id) {
          getStateList('', data.country.id, data.state);
          data.country = data.country.id;
        }
        if (data.state && data.state.id) {
          getCityList('', data.state.id, data.city);
          data.state = data.state.id;
        }
        if (data.city && data.city.id) {
          data.city = data.city.id;
        }
        ctrl.companyProfile = data;
        console.log('ctrl.companyProfile', ctrl.companyProfile);
        ctrl.companyProfileTemp = angular.copy(ctrl.companyProfile);
      },
      function(error) {
        ctrl.companyError = error;
        //getting all country data
        getCountryList('');
        console.log("error");
      });

    function getCountryList(searchString, country) {
      ctrl.isCountryLoading = true;
      console.log("Inside country list");
      var promise = $q.defer();
      GeoLocationSearch.getCountry({
        q: searchString
      }, function(data) {
        ctrl.companyCountryList = data.results;
        //checking if the country is present in the country list
        if (country && country.id) {
          var filteredCountry = $filter('filter')(ctrl.companyCountryList, { id: country.id }, true);
          if (filteredCountry.length == 0) {
            ctrl.companyCountryList.push(country)
          }
        }
        ctrl.isCountryLoading = false;
        promise.resolve(data.results);
      }, function(error) {
        console.log(error);
      });
      return promise
    }

    function getStateList(searchString, countryId, state) {
      var promise = $q.defer();
      GeoLocationSearch.getState({
        country: countryId,
        q: searchString
      }, function(data) {
        ctrl.companyStateList = data.results;
        //checking if the state is present in the state list
        if (state && state.id) {
          var filteredState = $filter('filter')(ctrl.companyStateList, { id: state.id }, true);
          if (filteredState.length == 0) {
            ctrl.companyStateList.push(state)
          }
        }
        promise.resolve(data.results);
      }, function(error) {
        console.log(error);
      });
      return promise
    }

    function getCityList(searchString, stateId, city) {
      var promise = $q.defer();
      GeoLocationSearch.getCity({
        state: stateId,
        q: searchString
      }, function(data) {
        ctrl.companyCityList = data.results;
        //checking if the city is present in the city list
        if (city && city.id) {
          var filteredCity = $filter('filter')(ctrl.companyCityList, { id: city.id }, true);
          if (filteredCity.length == 0) {
            ctrl.companyCityList.push(city)
          }
        }
        promise.resolve(data.results);
      }, function(error) {
        console.log(error);
      });
      return promise
    }


    function addProgramaticBlacklist(game, country) {
      ctrl.blacklist.serverError = '';
      var resource = GameSettings.addBlacklist({ game: game.id, country: country.id, status: 1 },
        function(data) {
          GameSettings.clearCache();
          ctrl.blacklist.game = null;
          ctrl.blacklist.country = null;
          ctrl.programaticBlacklist.results.unshift(data);
        },
        function(error) {
          ctrl.blacklist.serverError = error;
          console.log(error);
        });

      return resource.$promise;
    }

    function removeProgramaticBlacklist(blacklist) {
      var resource = GameSettings.removeBlacklist({
        status: 3,
        id: blacklist.id
      }, function(data) {
        GameSettings.clearCache();
        blacklist.status = data.status
      }, function(error) {
        blacklist.serverError = error;
        console.log(error);
      });
      return resource.$promise;
    }

    function addFloorecpm(ecpm) {
      ecpm.serverError = '';
      var resource = GameSettings.addFloorecpm({
        game: ecpm.game.id,
        country: ecpm.country.id,
        campaign_type: ecpm.campaignType.id,
        eCPM: ecpm.floorEcpm,
        status: 1
      }, function(data) {
        ctrl.floorEcpms.results.unshift(data);
        ecpm.game = null;
        ecpm.country = null;
        ecpm.campaignType = null;
        ecpm.floorEcpm = null;
        GameSettings.clearCache();
      }, function(error) {
        ecpm.serverError = error;
        console.log(error);
      });
      return resource.$promise;
    }

    function removeFloorecpm(ecpm) {
      ecpm.serverError = '';
      var resource = GameSettings.removeFloorecpmList({
        id: ecpm.id,
        status: 3
      }, function(data) {
        GameSettings.clearCache();
        ecpm.status = data.status
      }, function(error) {
        ecpm.serverError = error;
        console.log(error);
      });
      return resource.$promise;
    }

    function updateFloorecpm(ecpm) {
      ecpm.serverError = '';
      var resource = GameSettings.updateFloorecpmList({
        id: ecpm.id,
        eCPM: ecpm.eCPM
      }, function(data) {
        GameSettings.clearCache();
        ecpm.floorEcpm = data.eCPM;
        ecpm.status = 1;
        ecpm.isUpdateMode = false;
      }, function(error) {
        ecpm.serverError = error;
        console.log(error);
      });
      return resource.$promise
    }
    ctrl.instantMessangers = [{
      id: 0,
      label: 'Skype'
    }, {
      id: 1,
      label: 'Google Chat'
    }];

    User.get({
      id: $rootScope.me.id
    }, function(data) {
      //success
      ctrl.basicProfile = data;
      ctrl.basicProfileTemp = angular.copy(ctrl.basicProfile);
    }, function(error) {
      ctrl.basicProfileError = error;
    });


    User.getPaymentDetail({
        id: $rootScope.me.id
      }, function(data) {
        ctrl.paymentProfile = data;
        ctrl.paymentProfileTemp = angular.copy(ctrl.paymentProfile);
      },
      function(error) {
        ctrl.paymentProfileError = error;
      });

    Member.get({},
      function(data) {
        ctrl.teamMembers = data.results;
      },
      function(error) {
        ctrl.teamMembersError = error;
      });

    Invoicing.getInvoicesList({},
      function(data) {
        ctrl.invoices = data;
      },
      function(error) {
        ctrl.invoicesError = error;
        console.log(error);
      });

    function getRoleName(r) {
      switch (r) {
        case 0:
          return 'Admin';
        case 1:
          return 'Developer';
        case 2:
          return 'Tester';
      }
      return 'Select Role';
    };

    function isOperationAllowed(member) {
      if (member.isloading === true) {
        return false;
      }
      if ($rootScope.currentRole === 1) {
        if (member.publisher_role === 0) {
          return false;
        }
      }
      if ($rootScope.currentRole === 2) {
        return false;
      }
      if (member.email == $rootScope.me.email) {
        return false;
      }
      return true;
    };

    function addMember(member, teamManagementForm) {
      member.serverError = '';
      var resource = Member.save(member,
        function(data) {
          ctrl.newTeamMember = {};
          teamManagementForm.$setPristine();
          teamManagementForm.$setUntouched();
          ctrl.teamMembers.unshift({
            id: data.id,
            email: data.email,
            publisher_role: data.publisher_role,
            group: data.group
          });
        },
        function(error) {
          member.serverError = error;
        });
      return resource.$promise;
    };

    function editMember(teamMember, role) {
      teamMember.serverError = '';
      teamMember.isloading = true;
      var newMember = angular.copy(teamMember);
      newMember.publisher_role = role;
      console.log(newMember);
      Member.save(newMember, function(data) {
        console.log(data);
        teamMember.isloading = false;
        teamMember.publisher_role = data.publisher_role;
        teamMember.group = data.group;
      }, function(error) {
        console.log(error);
        teamMember.isloading = false;
        teamMember.serverError = error;
      })
    }

    function deleteMember(teamMember) {
      teamMember.serverError = '';
      var resource = Member.delete({
        id: teamMember.id
      }, function(data) {
        var index = ctrl.teamMembers.indexOf(teamMember);
        ctrl.teamMembers.splice(index, 1);
      }, function(error) {
        console.log(error);
        teamMember.serverError = error;
      });
      return resource.$promise;
    }

    function savePaymentProfile(paymentProfile) {
      console.log("paymentProfile", paymentProfile)
      ctrl.paymentProfileError = '';
      ctrl.paymentProfile = User.savePaymentDetail({
          id: $rootScope.me.id
        }, paymentProfile,
        function(data) {
          ctrl.paymentProfileTemp = angular.copy(ctrl.paymentProfile);
          toastr.success('Payment details saved successfully');
        },
        function(error) {
          ctrl.paymentProfileError = error;
        });
      return ctrl.paymentProfile.$promise;
    };

    function saveBasicProfile(basicProfile) {
      ctrl.basicProfileError = '';
      ctrl.basicProfile = User.save({
          id: $rootScope.me.id
        }, basicProfile,
        function(data) {
          //success
          ctrl.basicProfileTemp = angular.copy(basicProfile);
          toastr.success('User profile saved successfully');
        },
        function(error) {
          //error
          ctrl.basicProfileError = error;

        });
      return ctrl.basicProfile.$promise;
    };

    function saveCompanyProfile(companyProfile) {
      ctrl.companyError = '';
      var payload = angular.copy(companyProfile);
      if (companyProfile.country.id) {
        payload.country = companyProfile.country.id;
      }
      if (companyProfile.state.id) {
        payload.state = companyProfile.state.id;
      }
      if (companyProfile.city.id) {
        payload.city = companyProfile.city.id;
      }
      var resource = User.saveCompanyDetail({}, payload,
        function(data) {
          //success
          if (data.country && data.country.id) {
            data.country = data.country.id;
          }
          if (data.state && data.state.id) {
            data.state = data.state.id;
          }
          if (data.city && data.city.id) {
            data.city = data.city.id;
          }
          ctrl.companyProfile = data;
          ctrl.companyProfileTemp = angular.copy(ctrl.companyProfile);
          toastr.success('Company profile saved successfully');
        },
        function(error) {
          //error
          ctrl.companyError = error;
        });
      return resource.$promise;
    };

    function resetBasicProfile() {
      ctrl.basicProfile = angular.copy(ctrl.basicProfileTemp);
      ctrl.errMessage = '';
    };

    function resetCompanyProfile() {
      ctrl.companyProfile = angular.copy(ctrl.companyProfileTemp);
      ctrl.errMessage = '';
    }

    function resetPaymentProfile() {
      ctrl.paymentProfile = angular.copy(ctrl.paymentProfileTemp);
      ctrl.errMessage = '';
    };

    function isChangeBasicProfile(basicProfile) {
      if (angular.equals(basicProfile, ctrl.basicProfileTemp)) {
        return true;
      }
      return false;
    };

    function isChangePaymentProfile(paymentProfile) {
      if (angular.equals(paymentProfile, ctrl.paymentProfileTemp)) {
        return true;
      }
      return false;
    };

    function isCompanyProfileChanged(companyProfile) {

      if (angular.equals(companyProfile, ctrl.companyProfileTemp)) {
        return true;
      }
      return false;
    }


    function changeUserPassword(form) {
      ctrl.changePasswordError = null;
      $rootScope.messageResponse = null;
      var user = {};
      if (form.$valid) {
        form.isBusy = true;
        user.old_password = ctrl.changePassword.oldPassword;
        user.new_password = ctrl.changePassword.newPassword;
        var resource = User.changePassword(user, function(data) {
            ctrl.user = null;
            form.isBusy = false;

            $location.path('account/login').search({
              from: 'cgpwd'
            });
            $window.location.reload();
          },
          function(error) {
            $rootScope.errorMsg = error.data;
            ctrl.changePasswordError = error;
            form.isBusy = false;
            console.log(error.data.non_field_errors);
          });
        return resource.$promise
      }
    };

    function downloadInvoice(invoice) {
      invoice.isDownloading = true;
      Invoicing.getInvoiceLink({
        id: invoice.id
      }, function(data) {
        invoice.isDownloading = false;
        window.location.assign(data.url);
      }, function(error) {
        invoice.isDownloading = false;
        invoice.serverError = error;
        console.log(error);
      });
    }

    function approveInvoice(invoice) {
      //using $http because ng-resource doesnt support put without payload
      var promise = $http({ method: 'PUT', url: apiPrefix + 'invoices/update/' + invoice.id + '?status=2' }).then(function(res) {
          invoice.status = res.data.status;
          console.log(invoice);
        },
        function(error) {
          invoice.serverError = error;
          console.log(error);
        });
      return promise;
    }
  };
})();