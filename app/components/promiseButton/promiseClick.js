(function() {
  'use strict';

  angular.module('devPanelApp')
    .directive('promiseClick', promiseClick)

  function promiseClick() {
    return {
      restrict: 'A',
      scope: {
        promiseClick: '&promiseClick'
      },
      link: function(scope, element, attrs) {

        var orginalElementText = element.text();

        function updateUI(isBusy, msg) {
          if (isBusy == true) {
            element.attr('disabled', 'disabled');
            element.html('<i class="fa fa-spinner fa-spin"></i> ' + msg);
          } else {
            element.attr('disabled', null);
            element.html(msg);
          }
        }

        element.bind('click', function(e) {
          var elemText = orginalElementText.toLowerCase().replace(/(\r\n|\n|\r)/gm, "");
          elemText = elemText.replace(/ /g, '');
          var busyText = "Updating";
          if (elemText == "save") {
            busyText = "Saving";
          } else if (elemText == "fetch") {
            busyText = "Fetching";
          } else if (elemText == "cancel") {
            busyText = "Canceling";
          } else if (elemText == "reject") {
            busyText = "Rejecting"
          } else if (elemText == "save&complete") {
            busyText = "Completing"
          } else if (elemText == "request") {
            busyText = "Requesting"
          } else if (elemText == "add") {
            busyText = "Adding"
          } else if (elemText == "approve") {
            busyText = "Approving"
          } else if (elemText == "remove") {
            busyText = "Removing"
          } else if(elemText == "login") {
            busyText = "Login..."
          } else if(elemText == "requestchangeofpassword") {
            busyText = "Request change of password"
          }
          updateUI(true, busyText);
          var promise = scope.promiseClick();
          if (promise) {
            promise.then(function(d) {
              updateUI(false, orginalElementText);
            }, function(error) {
              updateUI(false, orginalElementText);
            });
          } else {
            //if there is no promise
            updateUI(false, orginalElementText);
          }
        });
      }
    };
  };

})();