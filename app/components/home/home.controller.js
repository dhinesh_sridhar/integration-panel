(function() {
  'use strict';

  angular.module('devPanelApp')
    .controller('HomeCtrl', ['$rootScope', '$q',
      '$filter', 'localStorageService', 'Lightbox', 'TableAnalytics', HomeCtrl
    ]);

  function HomeCtrl($rootScope, $q, $filter, localStorageServices, Lightbox, TableAnalytics) {
    var ctrl = this;

    ctrl.currentGameProfile = $rootScope.currentGameProfile;

    ctrl.cardValues = {
      revenue: 0,
      impression: 0,
      ctr: 0,
      ecpm: 0
    };

    ctrl.filterCallback = function(globalFilter) {
      console.log("filterCallback-globalFilter", globalFilter);
      _cardCallback(globalFilter);
      _graphCallback(globalFilter);
      _tableCallback(globalFilter);
    }


    ctrl.getCardDisplayData = function(type) {
      switch (type) {
        case 'revenue':
          return "Revenue";
        case 'impression':
          return "Impressions";
        case 'ctr':
          return "CTR";
        case 'ecpm':
          return "eCPM";
      }
    }


    ctrl.getGraphTypeName = function(type) {
      //console.log(type);
      switch (type) {
        case 0:
          return "Data Graph";
        case 1:
          return "World Map";
      }
    }

    ctrl.getGraphDataTypeName = function(type) {
      //console.log(type);
      switch (type) {
        case 0:
          return "Revenue";
        case 1:
          return "Impressions";
        case 2:
          return "CTR";
        case 3:
          return "eCPM";
        case 4:
          return "DAU";
        case 5:
          return "Ad-request";
      }
    }

    //change the type of graph
    ctrl.changeGraphType = function(type) {
      ctrl.graphType = type;
      ctrl.changeGraphDataType(ctrl.graphDataType);
    };

    ctrl.changeGraphDataType = function(type) {
      ctrl.graphError = false;
      console.log('changeGraphType', type, ctrl.graphType);
      ctrl.graphDataType = type;
      if (ctrl.graphType === 0) {
        if (type === 5) {
          if (ctrl.adRequestGraphError) {
            ctrl.graphError = true;
          } else {
            _drawGraph(_graphAdRequest);
          }
        } else {
          if (ctrl.dateGraphError) {
            ctrl.graphError = true;
          } else {
            _drawGraph(_graphRawData);
          }
        }
      } else {
        if (ctrl.graphDataType == 5) {
          if (ctrl.adRequestCoutryGraphError) {
            ctrl.graphError = true;
          } else {
            _formatdata(ctrl.AdRequestCoutryTrends.results);
          }
        } else {
          if (ctrl.countryGraphError) {
            ctrl.graphError = true;
          } else {
            _formatdata(ctrl.countrytrends.results);
          }
        }
      }
    };

    ctrl.tableType = 0;
    ctrl.gameProfile = $rootScope.gameProfiles;

    ctrl.changeTableType = function(type) {
      ctrl.tableType = type;
      _tableCallback(ctrl.globalFilter);
    }

    ctrl.getGraphTableName = function(type) {
      switch (type) {
        case 0:
          return 'Game'
          break;
        case 1:
          return 'Country'
          break;
        case 2:
          return 'Date'
      }

    }

    function _cardCallback(globalFilter) {
      ctrl.cardLoding = true;
      console.log("home callbacks " + globalFilter);
      if (globalFilter) {
        var cardValue = TableAnalytics.getGameWise(globalFilter, function(data) {
          ctrl.cardValues.revenue = data.agg.revenue || 0;
          ctrl.cardValues.impression = data.agg.impression || 0;
          ctrl.cardValues.ctr = data.agg.ctr || 0;
          ctrl.cardValues.ecpm = data.agg.ecpm || 0;
          ctrl.cardLoding = false;
        }, function(error) {
          ctrl.cardLoding = false;
          ctrl.cardError = error;
        });
        console.log("cardValue", cardValue);
      }
    }


    ctrl.allRegionsList = [];
    ctrl.regions = {};
    ctrl.graphType = ctrl.graphType || 0;
    ctrl.graphDataType = ctrl.graphDataType || 0;
    ctrl.filters = {};
    var _graphRawData = null;
    var _graphAdRequest = null;

    function _drawGraph(data) {

      if (data && data.results.length > 0) {
        ctrl.timeRange = {
          from: data.results[data.results.length - 1].key,
          till: data.results[0].key
        };


        var oneday = 86400; // in miliseconds
        var tickMap = [];
        var j = 0;
        var i = ctrl.timeRange.from;

        while (i <= ctrl.timeRange.till) {
          tickMap[j] = {
            key: i
          };
          i = i + oneday;
          j++;
        }

        console.log("The Thick map is ", data.results, data.results.length, " ", tickMap.length);
        var data1 = {};
        data1.results = data.results.reverse();

        for (i = 0; i < data1.results.length; i++) {

          if (data1.results[i].key !== tickMap[i].key) {
            if (ctrl.graphDateType === 5) {
              data1.results.splice(i, 0, { key: tickMap[i].key, adrequests: 0 })
            } else if (ctrl.graphDateType === 4) {
              data1.results.splice(i, 0, { key: tickMap[i].key, daily_active_users: 0 })
            } else {
              data1.results.splice(i, 0, { key: tickMap[i].key, revenue: 0, impression: 0, ctr: 0, ecpm: 0 })
            }


          }

        }
        data = data1;
        data.results = data.results.reverse();


        var _gameData = {};
        if (ctrl.graphDataType === 0) {
          _gameData.key = 'revenue';
        } else if (ctrl.graphDataType === 1) {
          _gameData.key = 'impression';
        } else if (ctrl.graphDataType === 2) {
          _gameData.key = 'ctr';
        } else if (ctrl.graphDataType === 3) {
          _gameData.key = 'ecpm';
        } else if (ctrl.graphDataType === 4) {
          _gameData.key = 'daily_active_users';
        } else if (ctrl.graphDataType === 5) {
          _gameData.key = 'adrequests';
        }
        _gameData.values = [];
        //GraphStructure is array of 2D array as [[epochNumer, data],...]
        var data1 = angular.copy(data.results);
        data1 = data1.reverse();

        var graphData = [];
        var labels = [];
        var values = [];
        angular.forEach(data1, function(value, key) {
          values.push(value[_gameData.key]);
          labels.push(value.label);
          var graphData = [];
          console.log(value, _gameData.key, value[_gameData.key]);
          graphData.push({
            key: value.key * 1000,
            values: value[_gameData.key].toFixed(2)

          });
          _gameData.values.push(graphData);

        });
        var gameData1 = [];

        gameData1.push(_gameData);

        var yAxisValues = function() {
          var thickValues = [];
          console.log('force values', values)

          switch (ctrl.graphDataType) {
            case 0:
              if (values.length === 0 || parseFloat(Math.max.apply(Math, values).toFixed(2)) === 0.00 || parseFloat(Math.max.apply(Math, values).toFixed(2)) <= 10.00) {
                return [0, 10];
              }
              break;
            case 1:
              if (values.length === 0 || parseFloat(Math.max.apply(Math, values).toFixed(2)) === 0.00 || parseFloat(Math.max.apply(Math, values).toFixed(2)) <= 10000.00) {
                return [0, 10000];
              }
              break;
            case 2:
              if (values.length === 0 || parseFloat(Math.max.apply(Math, values).toFixed(2)) === 0.00 || parseFloat(Math.max.apply(Math, values).toFixed(2)) <= 1.00) {
                return [0, 1];
              }
              break;
            case 3:
              if (values.length === 0 || parseFloat(Math.max.apply(Math, values).toFixed(2)) === 0.00 || parseFloat(Math.max.apply(Math, values).toFixed(2)) <= 1.00) {
                return [0, 1];
              }
              break;
            case 5:
              if (values.length === 0 || parseFloat(Math.max.apply(Math, values).toFixed(2)) === 0.00 || parseFloat(Math.max.apply(Math, values).toFixed(2)) <= 10000.00) {
                return [0, 10000];
              }
          }


          var maxValue = Math.round(parseFloat(Math.max.apply(Math, values).toFixed(2)));

          maxValue = maxValue + maxValue / 5;
          console.log('maxValue', maxValue);
          var divident = maxValue / 5;
          console.log('divident', divident);
          var thickValues = [];
          var thickvalue = 0;
          for (var j = 0; j <= 5; j++) {
            thickValues.push(thickvalue);
            thickvalue = thickvalue + divident;
          }
          console.log('thickValues', thickValues)
          return thickValues;
        }


        /////graph started///
        console.log("The Graph data", gameData1);
        ctrl.graphData = gameData1;
        ctrl.graphData[0]['key'] = ctrl.getGraphDataTypeName(ctrl.graphDataType);
      } else {
        ctrl.graphData = [];
        var yAxisValues = function() {
          return [0, 10];
        }
      }

      var dateDiff = ctrl.globalFilter.till_date - ctrl.globalFilter.from_date;
      var weekSeconds = 604800;
      console.log(dateDiff);
      console.log("Math.max.apply(Math,values),Math.min.apply(Math,values)", Math.max.apply(Math, values), Math.min.apply(Math, values), values);
      ctrl.graphOptions = {
        chart: {
          type: (weekSeconds > dateDiff) ? 'discreteBarChart' : 'lineChart',
          height: 350,
          color: (weekSeconds > dateDiff) ? ['#bbd3e7', '#acc9e1', '#9dbfdc', '#75a5ce', '#649ac8', '#4d8bc0', '#337ab7'] : ['#337ab7'],
          forceY: yAxisValues(),
          showValues: true,
          margin: {
            top: 20,
            right: 30,
            bottom: 50,
            left: 65
          },
          x: function(d) {
            return d[0].key;
          },
          y: function(d) {
            return d[0].values;
          },
          axisLeft: 10,
          duration: 100,
          useInteractiveGuideline: true,
          clipVoronoi: false,
          xAxis: {
            orient: "bottom",
            //rotateLabels: 1,
            //tickPadding:25,
            tickFormat: function(d) {
              if (isNaN(d)) {
                return d;
              }
              var d1 = new Date(d);
              return $filter('date')(d1, 'dd-MMM');
              return d;
            },
            tickValues: function(values) {
              var tickValues = [];
              console.log("values", values[0]['values'][0][0]);
              for (var i = 0; i < values[0].values.length; i = i + _incrementCounter()) {
                tickValues.push(values[0]['values'][i][0]['key'])
              }
              //tickValues.pop();
              //tickValues.shift();
              console.log("tickValues", tickValues);

              function _incrementCounter() {
                if (values[0].values.length > 15) {
                  return Math.round(values[0].values.length / 15)
                } else {
                  return 1;
                }
              }
              console.log('tickValues12 ', tickValues);
              return tickValues;
            },
            showMaxMin: false
          },
          yAxis: {
            axisLabel: ctrl.getGraphDataTypeName(ctrl.graphDateType),
            tickFormat: function(d) {
              if (ctrl.graphDataType === 0 || ctrl.graphDataType === 3) {
                return $filter('currency')(d, '$');
              } else if (ctrl.graphDataType === 2) {
                return $filter('number')(d, '2') + '%';
              } else {
                return $filter('number')(d, '');
              }
              return d;
            },
            axisLabelDistance: 0
          }
        }
      };
    };

    function _graphCallback(globalFilter) {

      ctrl.graphLoding = true;
      console.log("ctrl.globalFilter", globalFilter);
      if (globalFilter) {
        ctrl.globalFilter = globalFilter;
        ctrl.dateGraphError = '';
        ctrl.adRequestGraphError = '';
        ctrl.countryGraphError = '';
        ctrl.adRequestCoutryGraphError = '';
        var dateAnalyticResource = TableAnalytics.getDateWise(globalFilter);
        var adRequestResource = TableAnalytics.getAdrequestData(globalFilter);
        var countryResource = TableAnalytics.getCountryWise(globalFilter);
        var adRequestCountryResource = TableAnalytics.getAdrequestCountry(globalFilter);

        var totalPromise = $q.all([dateAnalyticResource.$promise, adRequestResource.$promise, countryResource.$promise, adRequestCountryResource.$promise]);

        totalPromise.then(function(data) {
          data[0].results = $filter('orderBy')(data[0].results, 'key', true);
          _graphRawData = data[0];
          ctrl.gametrends = data[0];
          data[1].results = $filter('orderBy')(data[1].results, 'key', true);
          _graphAdRequest = data[1];
          ctrl.countrytrends = data[2];
          ctrl.AdRequestCoutryTrends = data[3];
          ctrl.changeGraphType(ctrl.graphType);
          ctrl.graphLoding = false;

        }, function(error) {
          ctrl.dateGraphError = error[0];
          ctrl.adRequestGraphError = error[1];
          ctrl.countryGraphError = error[2];
          ctrl.adRequestCoutryGraphError = error[3];
          ctrl.changeGraphType(ctrl.graphType);
          ctrl.graphLoding = false;
          console.log(error);
        })
      }
    }

    function _formatdata(dataset) {
      console.log('formatdata');
      console.log('dataSet ', dataset);

      ctrl.datalist = [];
      ctrl.datalist.push(['Country', ctrl.getGraphDataTypeName(ctrl.graphDataType)]);
      for (var i = 0; i < dataset.length; i++) {
        if (dataset[i].countryKey != 0) {
          var data = [];
          data.push(dataset[i].country_name);
          var value;
          if (ctrl.graphDataType == 0) {
            value = dataset[i].revenue;
          } else if (ctrl.graphDataType == 1) {
            value = dataset[i].impression;
          } else if (ctrl.graphDataType == 2) {
            value = dataset[i].ctr;
          } else if (ctrl.graphDataType == 3) {
            value = dataset[i].ecpm;
          } else if (ctrl.graphDataType == 5) {
            value = dataset[i].adrequests;
          }

          data.push(value);

          ctrl.datalist.push(data);

          ctrl.allRegionsList.push(dataset[i].country_code);
        }
      };

      var chart1 = {};
      chart1.type = 'GeoChart';
      chart1.data = ctrl.datalist;
      chart1.allRegionsList = ctrl.allRegionsList;
      chart1.regions = ctrl.regions;
      chart1.options = {
        region: 'world',
        colorAxis: { colors: ['#cad8fa', '#103aa4'] }, // Map Colors
        datalessRegionColor: '#e2e2e2',
        height: 380,
        legend: { numberFormat: '.##' % '' },
      };

      ctrl.chart = chart1;
    }


    function _tableCallback(globalFilter) {
      if (globalFilter) {
        ctrl.tableError = '';
        var typeTable = null;
        ctrl.globalFilter = globalFilter;
        ctrl.dataLoding = true;
        switch (ctrl.tableType) {
          case 0:
            typeTable = 'getGameWise';
            break;
          case 1:
            typeTable = 'getCountryWise';
            break;
          case 2:
            typeTable = 'getDateWise';
        };

        TableAnalytics[typeTable](globalFilter, function(data) {
          for (var i = 0; i < data.results.length; i++) {
            var r = data.results[i];
            data.results[i].item_id = data.results[i].key;
          }
          ctrl.gameTable = data;
          ctrl.dataLoding = false;
        }, function(error) {
          ctrl.tableError = error;
          ctrl.dataLoding = false;
        });
      }
    }
  };
})();