'use strict';

google.charts.load('current', { 'packages': ['geochart'] });

angular.module('devPanelApp')
  .directive('googleChart', ['$window', function($window) {
    return {
      restrict: 'EA',
      scope: {
        chart: '=chart'
      },
      link: function(scope, element, attrs) {
        var currentRegion;
        var geochart = new google.visualization.GeoChart(
          document.getElementById('chart_div'));

        scope.$watch('chart', function(nv) {
          if (nv) {
            console.log('watch');

            var data = google.visualization.arrayToDataTable(scope.chart.data);
            var options = scope.chart.options;

            geochart.draw(data, options);

            google.visualization.events.addListener(geochart, 'regionClick', function(eventData) {
              currentRegion = eventData.region;
              options.region = eventData.region;
              options.resolution = 'provinces';

              // Add Results for Individual State
              // Format needs to match what is below so that it locates the correct position
              // Additional information can be added to array
              // Uses first value in 2nd column to determine scale for markers
              if (scope.chart.allRegionsList.indexOf(currentRegion) > -1 && Object.prototype.toString.call(scope.chart.regions[currentRegion]) === '[object Array]') {

                var datalist = scope.chart.regions[currentRegion];
                var data = google.visualization.arrayToDataTable(datalist);

                geochart.draw(data, options);
              }
            });
            angular.element($window).bind('resize', function() {
              geochart.draw(data, options);
            });
          }
        });


      }
    };
  }]);
