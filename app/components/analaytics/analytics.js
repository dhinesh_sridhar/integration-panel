(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('TableAnalytics', ['$resource', '$cacheFactory', 'dataApiPrefix', TableAnalytics]);

  function TableAnalytics($resource, $cacheFactory, dataApiPrefix) {
    var cacheService = $cacheFactory('TableAnalytics');

    var resource = $resource(dataApiPrefix + 'analytics/:tabletype/:subTable', {}, {
      getGameWise: {
        method: 'GET',
        params: { tabletype: 'game' },
        ignoreLoadingBar: true,
        cache: cacheService
      },
      getCountryWise: {
        method: 'GET',
        params: { tabletype: 'country' },
        ignoreLoadingBar: true,
        cache: cacheService
      },
      getDateWise: {
        method: 'GET',
        params: { tabletype: 'date' },
        ignoreLoadingBar: true,
        cache: cacheService
      },
      getDirectCampaigns: {
        method: 'GET',
        params: { tabletype: 'direct' },
        cache: cacheService
      },
      getDauData: {
        method: 'GET',
        params: { tabletype: 'dau' },
        ignoreLoadingBar: true,
        cache: cacheService
      },
      getAdrequestData: {
        method: 'GET',
        params: {
          tabletype: 'adrequests',
          subTable: 'date'
        },
        ignoreLoadingBar: true,
        cacheService: cacheService
      },
      getAdrequestCountry: {
        method: 'GET',
        params: {
          tabletype: 'adrequests',
          subTable: 'country'
        },
        ignoreLoadingBar: true,
      }
    });
    return resource;
  };
})();
