(function() {
  'use strict';
  angular.module('devPanelApp')
    .controller('GameAnalyticsCtrl', ['$routeParams', '$filter', '$rootScope', 'TableAnalytics', gameAnalyticsCtrl]);

  function gameAnalyticsCtrl($routeParams, $filter, $rootScope, TableAnalytics) {
    var ctrl = this;

    ctrl.currentGameProfile = $rootScope.currentGameProfile;
    ctrl.currentGameId = $routeParams.gameId;
    ctrl.date = {
      startDate: moment().subtract(7, "days"),
      endDate: moment()
    };
    var filter = {};
    ctrl.options = {
      applyClass: 'btn-green',
      locale: {
        applyLabel: "Apply",
        fromLabel: "From",
        format: "D-MMMM-YY",
        toLabel: "To",
        cancelLabel: 'Cancel',
        customRangeLabel: 'Custom range'
      },
      opens: "left",
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 2 Days': [moment().subtract(1, 'days'), moment()],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()]
      },
      eventHandlers: { 'apply.daterangepicker': _dateChange }
    };

    _dateChange();

    function _dateChange() {
      ctrl.graphError = '';
      ctrl.graphLoding = true;
      filter.from_date = dateToEpoch(ctrl.date.startDate.clone().toDate()); // convrting moment to date object
      filter.till_date = dateToEpoch(ctrl.date.endDate.clone().toDate());
      filter.game = ctrl.currentGameId;
      TableAnalytics.getDauData(filter,
        function(data) {
          var tempData = data;
          ctrl.graphLoding = false;
          _drawGraph(tempData);
        },
        function(error) {
          ctrl.graphError = error;
          ctrl.graphLoding = false;
          console.log(error);
        });
    }


    function getCsvIds(arrayObj) {
      if ((arrayObj) && (arrayObj.length !== 0)) {
        return (arrayObj || []).map(function(elem) {
          return elem.id;
        }).join(",");
      }
      return undefined;
    };

    function dateToEpoch(dateObj) {
      if (dateObj) {
        return parseInt((dateObj.getTime() / 1000) - (dateObj.getTimezoneOffset() * 60));
      }
      return undefined;
    }

    function _drawGraph(data) {

      if (data && data.results.length > 0) {
        ctrl.timeRange = {
          from: data.results[data.results.length - 1].key,
          till: data.results[0].key
        };


        var oneday = 86400; // in miliseconds
        var tickMap = [];
        var j = 0;
        var i = ctrl.timeRange.from;

        while (i <= ctrl.timeRange.till) {
          tickMap[j] = {
            key: i
          };
          i = i + oneday;
          j++;
        }

        console.log("The Thick map is ", data.results.length, " ", tickMap.length);
        var data1 = {};
        data1.results = data.results.reverse();

        for (i = 0; i < data1.results.length; i++) {
          if (data1.results[i].key !== tickMap[i].key) {
            data1.results.splice(i, 0, { key: tickMap[i].key, daily_active_users: 0 })
          }
        }
        data = data1;
        data.results = data.results.reverse();


        var _gameData = {};
        _gameData.key = 'daily_active_users';

        _gameData.values = [];
        //GraphStructure is array of 2D array as [[epochNumer, data],...]
        var data1 = angular.copy(data.results);
        data1 = data1.reverse();

        var graphData = [];
        var labels = [];
        var values = [];
        angular.forEach(data1, function(value, key) {
          labels.push(value.label);
          var graphData = [];
          values.push(value[_gameData.key]);
          console.log(value, _gameData.key, value[_gameData.key]);
          graphData.push({
            key: value.key * 1000,
            values: value[_gameData.key]

          });
          _gameData.values.push(graphData);

        });
        var gameData1 = [];

        var maxValue = function() {
          if (Math.max.apply(Math, values) === 0) {
            return 1
          } else {
            return (Math.max.apply(Math, values) + (Math.max.apply(Math, values) / 8)); // adding values to make padding in graph, 8 is constnant used by d3 on yaxis thick value
          }
        }

        console.log("The maxValue is ", maxValue());

        gameData1.push(_gameData);


        /////graph started///
        console.log("The Graph data", gameData1);
        ctrl.graphData = gameData1;
        ctrl.graphData[0]['key'] = 'DAU'
      } else {
        ctrl.graphData = [];
        var maxValue = function() {
          return 1;
        }
      }

      ctrl.graphOptions = {
        chart: {
          type: 'lineChart',
          height: 350,
          forceY: [0, maxValue()],
          margin: {
            top: 20,
            right: 20,
            bottom: 50,
            left: 65
          },
          x: function(d) {
            return d[0].key;
          },
          y: function(d) {
            return d[0].values;
          },
          duration: 100,
          useInteractiveGuideline: true,
          clipVoronoi: false,

          xAxis: {
            axisLabel: 'Date',
            tickFormat: function(d) {
              if (isNaN(d)) {
                return d;
              }
              var d1 = new Date(d);
              return $filter('date')(d1, 'dd-MMM');
              return d;
            },
            tickValues: function(values) {
              var tickValues = [];
              console.log("values", values[0]['values'][0][0]);
              for (var i = 0; i < values[0].values.length; i = i + _incrementCounter()) {
                tickValues.push(values[0]['values'][i][0]['key'])
              }
              console.log("tickValues", tickValues);

              function _incrementCounter() {
                if (values[0].values.length > 15) {
                  return Math.round(values[0].values.length / 15)
                } else {
                  return 1;
                }
              }
              return tickValues;
            },
            showMaxMin: false
          },
          yAxis: {
            tickFormat: function(d) {
              return $filter('number')(d, '');
            },
            axisLabelDistance: 0
          }
        }
      };
    };
  };
})();