(function() {
  'use strict';
  angular.module('devPanelApp')
    .factory('s3credentials', ['$resource', 'apiPrefix', s3credentials]);
  angular.module('devPanelApp')
    .factory('sdkdownload', ['$resource', sdkdownload]);

  function s3credentials($resource, apiPrefix) {
    var s3credentials = $resource(apiPrefix + '/games/:gameId/apk/s3-policy', {
      gameId: '@game_profile'
    }, {
      get: {
        method: 'GET'
      },
    });
    return s3credentials;
  }


  function sdkdownload($resource) {
    var apiPrefix = 'https://api.github.com/repos/GreedyGame/'
    var sdkdownload = $resource(apiPrefix + ':type/releases/latest', {
      type: '@type'
    }, {
      getsdk: {
        method: 'GET',
        headers: { "noauth": "1" }
      },

    });
    return sdkdownload;
  };
})();
