(function() {
  'use strict';

  angular.module('devPanelApp')
    .factory('UnitCreation', ['$rootScope', '$q', 'socketPrefix','socketPostbackPrefix', UnitCreation]);

  function UnitCreation($rootScope, $q, socketPrefix, socketPostbackPrefix) {

    var taskObj = {};
    var socket = null;

    function connectSocket() {
      if (!socket) {
        console.log('reconnection');
        socket = new io(socketPrefix, {
          //reconnection: false,
          autoConnect: true,
          forceNew: true
        });
      }
    }

    $rootScope.$watch('me', function(nv) {
      if (nv && nv.id) {
        connectSocket();

        socket.on('connect', function() {
          console.log('connect');
          socket.emit('join', {
            user: nv.id
          });
        });

        socket.on('disconnect', function() {
          console.log('disconnect');
        });

        socket.on('jobResponse', function(msg) {
          console.log('on jobResponse');
          console.log('jobResponse', JSON.stringify(msg));
          var jobId = msg.job_id;
          if (jobId) {

            var deferred = taskObj[jobId].deferred;
            if (msg.error) {
              var errorFn = taskObj[jobId].errorFn;
              if (errorFn) {
                errorFn(msg);
              }
              deferred.reject(msg);
            } else {
              var successFn = taskObj[jobId].successFn;
              if (successFn) {
                successFn(msg);
              }
              deferred.resolve(msg);
            }
            //delete taskObj[taskId];
          }
        });
      } else if (socket) {
        socket.disconnect();
        socket = null;
      }
    });


    function _generateUUID() {
      var d = new Date().getTime();
      var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
      });
      return uuid;
    };

    function _createNativeUnit(parameters, successFn, errorFn) {

      connectSocket();

      var gameId = parameters.gameId;
      var demandId = parameters.demandId;
      var adgroupId = parameters.adgroupId;
      var job_id = _generateUUID();

      var autoData = {
        game_id: gameId,
        demand_id: demandId,
        adgroup_id: adgroupId,
        postback: socketPostbackPrefix+ 'worker/postback'
      };

      console.log(autoData);

      socket.emit('job', {
        user: $rootScope.me.id,
        job_id: job_id,
        worker: "dummycreate",
        task: autoData
      });

      var deferred = $q.defer();

      taskObj[job_id] = {
        successFn: successFn,
        errorFn: errorFn,
        deferred: deferred,
      };

      window.taskObj = taskObj;

      return deferred;
    };

    function _processPSDPlacement(parameters, successFn, errorFn) {

      connectSocket();

      console.log("process psd", parameters);
      var game_id = parameters.gameId;
      var unit_id = parameters.unitId;
      var psd_url = parameters.psdUrl;
      var job_id = _generateUUID();

      socket.emit('job', {
        user: $rootScope.me.id,
        job_id: job_id,
        worker: "process_psd",
        task: {
          game: game_id,
          unit: unit_id,
          psd: psd_url,
          postback: socketPostbackPrefix+ 'worker/postback'
        }
      });

      var deferred = $q.defer();

      taskObj[job_id] = {
        successFn: successFn,
        errorFn: errorFn,
        deferred: deferred,
      };

      return deferred;
    };

    return {
      createNativeUnit: _createNativeUnit,
      processPSD: _processPSDPlacement
    }
  };
})();
