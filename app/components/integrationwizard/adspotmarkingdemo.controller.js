(function() {
  'use strict';
  angular.module('devPanelApp')
    .controller('adSpotmarkingDemoCtrl', ['$uibModalInstance', 'adspotmarkingType', adSpotmarkingDemoCtrl]);

  function adSpotmarkingDemoCtrl($uibModalInstance, adspotmarkingType) {
    var ctrl = this;
    ctrl.adspotmarkingType = adspotmarkingType;
    ctrl.close = close;

    function close() {
      $uibModalInstance.dismiss('cancel');
    };
  };
})();
