(function() {
  'use strict';

  angular.module('devPanelApp')
    .directive('prePrism', ['$compile', '$timeout', prePrism]);

  function prePrism($compile, $timeout) {
    return {
      restrict: 'E',
      scope: {
        type: '@',
        source: '=',
        disableHighlighting: '@'
      },
      link: function(scope, element) {
        var timeout;
        scope.$watch('source', function(value) {
          if (!value) return;
          element.html('<pre class="line-numbers" style="max-height:500px;"><code>{{ source }}</code></pre>');
          $compile(element.contents())(scope);
          var code = element.find('code')[0];
          code.className = 'language-' + scope.type;
          if (scope.disableHighlighting !== 'true') {
            timeout = $timeout(function() {
              Prism.highlightElement(code);
            }, 0, false);
          } else {
            element.find('pre')[0].className = 'language-' + scope.type + ' line-numbers';
          }
        });

        scope.$on('$destroy', function() {
          $timeout.cancel(timeout);
        });
      }
    };
  };
})();
