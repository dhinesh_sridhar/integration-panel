(function() {
  'use strict';
  angular.module('devPanelApp')
    .directive('setClassAtBottom', ['$window', '$timeout', setClassAtBottom]);

  function setClassAtBottom($window, $timeout) {
    var $win = angular.element($window);
    return {
      restrict: 'A',
      scope: {
        setIf: '=ngSetIf'
      },
      link: function(scope, element, attrs) {
        scope.$watch('setIf', function(nv) {
          $timeout(function() {
            // This code runs after the DOM renders
            var topClass = attrs.setClassAtBottom;
            var ele = element[0];
            var box = ele.getBoundingClientRect();
            if (box.top - 650 > $window.pageYOffset) {
              element.addClass(topClass);
            } else {
              element.removeClass(topClass);
            }
            $win.on('scroll', function(e) {
              // console.log(box.top, $window.pageYOffset);
              if (box.top - 650 > $window.pageYOffset) {
                element.addClass(topClass);
              } else {
                element.removeClass(topClass);
              }
            });
          });
        });
      }
    };
  };
})();
