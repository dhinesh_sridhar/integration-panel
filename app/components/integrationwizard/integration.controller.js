(function() {
  'use strict';
  angular.module('devPanelApp')
    .controller('IntegrationWizrdCtrl', ['$rootScope', '$routeParams', '$location', 'toastr',
      'GameProfile', 'Gamedetails',
      'UnitCreation', 'Unit', 'SdkManual', 'Upload', '$uibModal', 'apiPrefix', '$filter', 'Device', 's3credentials', 'sdkdownload', 'Lightbox', 'Faq', IntegrationWizrdCtrl
    ]);

  function IntegrationWizrdCtrl($rootScope, $routeParams, $location, toastr, GameProfile, Gamedetails, UnitCreation, Unit,
    SdkManual, Upload, $uibModal, apiPrefix, $filter, Device, s3credentials, sdkdownload, Lightbox, Faq) {
    var ctrl = this;
    ctrl.gameId = $routeParams.gameId;
    ctrl.activeStep = parseInt($routeParams.step);
    ctrl.errorMsg = [];
    ctrl.activeSubStep = $routeParams.substep;
    ctrl.faqs = [];
    var floatUnitsLoaded = false;
    var nativeUnitsLoaded = false;
    var gameProfileLoaded = false;
    ctrl.gameGenres = [];
    ctrl.newGenre = {};
    ctrl.collapse = true;


    ctrl.ChangeEvent = ChangeEvent;
    ctrl.subclick = subclick;

    ctrl.closeAlert = closeAlert;
    ctrl.click = click;
    ctrl.getLeastStepToDo = getLeastStepToDo;
    ctrl.previousProcess = previousProcess;
    ctrl.nextProcess = nextProcess;
    ctrl.isNextProcessEnabled = isNextProcessEnabled;
    ctrl.uploadUnits = uploadUnits;


    initCommon();

    function ChangeEvent(targetElement, scope) {
      console.log("Change Event called");
      console.log(targetElement); //The target element
      console.log(this); //The IntroJS object
    };

    function loadSubmodule() {
      switch (ctrl.activeStep) {
        case 1:
          initStep1();
          break;
        case 2:
          initStep2();
          break;
        case 3:
          initStep3();
          break;
        case 4:
          initStep4();
          break;
        case 5:
          initStep5();
          break;
      }
    }

    function _initializeGamePlatforms(platforms) {
      platforms = platforms || [];
      var playStore = (($filter('filter')(platforms, { id: 1 })).length > 0) ? ($filter('filter')(platforms, { id: 1 }))[0] : { id: 1, name: 'Google Play Store', url: '' }
      var amazon = (($filter('filter')(platforms, { id: 2 })).length > 0) ? ($filter('filter')(platforms, { id: 2 }))[0] : { id: 2, name: 'Amazon', url: '' }
      var apps9 = (($filter('filter')(platforms, { id: 3 })).length > 0) ? ($filter('filter')(platforms, { id: 3 }))[0] : { id: 3, name: '9 Apps', url: '' }
      var gamePlatforms = [];
      gamePlatforms.push(playStore);
      gamePlatforms.push(amazon);
      gamePlatforms.push(apps9);
      return gamePlatforms;
    }

    function initCommon() {
      var activeStep = angular.copy(ctrl.activeStep);
      var activeSubStep = angular.copy(ctrl.activeSubStep);
      //doing this, since ad-spot marking is commented as of now.
      if (activeStep == 2 && activeSubStep == 'c' || activeSubStep == 'd') {
        activeSubStep = String.fromCharCode(activeSubStep.charCodeAt(0) + 2);
      }
      ctrl.faqError = '';
      Faq.get({
        tags: activeStep + '-' + activeSubStep
      }, function(data) {
        ctrl.faqs = data.results;
      }, function(error) {
        ctrl.faqError = error;
        console.log(error);
      });
      GameProfile.getGenresList({}, function(data) {
        console.log(data);
        ctrl.gameGenres = data.results;
      }, function(error) {
        console.log(error);
      });


      if (ctrl.gameId === 'new') {
        ctrl.gameProfile = {};
        ctrl.gameDetails = {};
        ctrl.gameDetails.ageGroup = {};
        ctrl.gameProfile.platforms = _initializeGamePlatforms();
        ctrl.gameProfileCopy = {};
        //calling submodule load method
        loadSubmodule();
      } else if ($rootScope.currentGameProfile) {
        $rootScope.currentGameProfile.$promise.then(function(data) {
          data.platforms = _initializeGamePlatforms(data.platforms);
          ctrl.gameProfile = data;
          gameProfileLoaded = true;
          ctrl.gameProfileCopy = angular.copy(ctrl.gameProfile);
          var sdkType = null;
          switch (ctrl.gameProfile.engine) {
            case 0:
              sdkType = 'unity-plugin'
              break;
            case 1:
              sdkType = 'cocos2dx-plugin'
              break;
            case 2:
              sdkType = 'andriod-native-plugin'
          }
          if (nativeUnitsLoaded && floatUnitsLoaded && gameProfileLoaded) {
            //calling submodule load method
            loadSubmodule();
          }
        });

        Unit.get({
          gameId: ctrl.gameId,
          unit_type: 0
        }, function(data) {
          ctrl.nativeUnits = data;
          nativeUnitsLoaded = true;

          if (nativeUnitsLoaded && floatUnitsLoaded && gameProfileLoaded) {
            //calling submodule load method
            loadSubmodule();
          }
        }, function(error) {
          ctrl.nativeUnitsError = error;
          console.log(error);
        });

        Unit.get({
          gameId: ctrl.gameId,
          unit_type: 1
        }, function(data) {
          ctrl.floatUnits = data;
          floatUnitsLoaded = true;
          if (nativeUnitsLoaded && floatUnitsLoaded && gameProfileLoaded) {
            //calling submodule load method
            loadSubmodule();
          }
        }, function(error) {
          ctrl.floatUnitsError = error;
          console.log(error);
        });
      }

      ctrl.engineName = function(n) {
        if (n == 0) {
          return "Unity3d";
        } else if (n == 1) {
          return "Cocos2d-x";
        } else if (n == 2) {
          return "Native Android";
        }
      };

      ctrl.isGameProfileChanged = function() {
        return angular.equals(ctrl.gameProfile, ctrl.gameProfileCopy) == false;
      };
      ctrl.isGameDetailChanged = function() {
        return angular.equals(ctrl.gameDetails, ctrl.gameDetailsCopy) == false;
      };
    };

    function subclick(step) {
      $location.path("/" + ctrl.gameId + "/wizard/" + ctrl.activeStep + "-" + step);
    };

    ctrl.gameProfilePlatforms = {};

    function initStep1() {
      console.log("initStep1")
      ctrl.gameDetails = {};
      ctrl.saveGameDetails = saveGameDetails;
      ctrl.saveGame = saveGame;
      ctrl.getPlaystoreInfo = getPlaystoreInfo;
      ctrl.checkPlatformUrl = checkPlatformUrl;
      ctrl.isGameDetailsValid = isGameDetailsValid;

      if ($rootScope.currentGameProfile && $rootScope.currentGameProfile.$promise) {
        $rootScope.currentGameProfile.$promise.then(function(data) {
          //checking this for data unavailablity and new game screen
          if (data.platforms && data.platforms[0] && data.platforms[0].url) {
            ctrl.getPlaystoreInfo(data.platforms[0].url);
          }
        });
      }

      ctrl.gameDetails.ageGroup = {};
      if (ctrl.gameId !== 'new') {

        Gamedetails.get({ gameid: ctrl.gameId },
          function(data) {
            ctrl.gameDetails.demography = data.gender;

            for (var i = 0; i < ((data.age) ? data.age.length : 0); i++) {
              var min_age = data.age[i].min_age;
              ctrl.gameDetails.ageGroup['agegroup' + min_age] = data.age[i].value || 0;
            }
            ctrl.gameDetailsCopy = angular.copy(ctrl.gameDetails);
          },
          function(error) {
            ctrl.gameDetailsError = error;
          });
      }

      function _getUrlParameterByName(name, url) {
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
      }

      function _getPathFromUrl(url) {
        return url.split(/[?#]/)[0];
      }

      function _isPlayStoreURl(url) {
        if (url.split("?")[0] === 'https://play.google.com/store/apps/details') {
          return true;
        }
        return false;
      }

      function _getHostName(url) {
        var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
        if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
          return match[2];
        } else {
          return null;
        }
      }

      function getPlaystoreInfo(url) {
        console.log('inside get playstore info', url);
        ctrl.playstore = null;
        var bundleId = _getUrlParameterByName('id', url);
        console.log("inside getPlaystoreInfo123", url, bundleId);
        console.log('_isPlayStoreURl(url) && bundleId', (_isPlayStoreURl(url) && bundleId) ? 'true' : 'false')
        if (_isPlayStoreURl(url) && bundleId) {
          ctrl.isPlaystoreLoading = true;
          ctrl.playStoreError = null;
          var resource = GameProfile.getPlayStoreInfo({ pkg: bundleId },
            function(data) {
              ctrl.isPlaystoreLoading = false;
              ctrl.playstore = data;
              (!ctrl.gameProfile.title) ? ctrl.gameProfile.title = data.title: '';
            },
            function(error) {
              ctrl.isPlaystoreLoading = false;
              ctrl.playStoreError = error;
              console.log(error);
            });
          return resource.$promise;
        } else {
          ctrl.playStoreError = { data: ['Please enter a valid Playstore URL.'] };
          return false;
        }
      }

      function checkPlatformUrl(type, url) {
        if (url) {
          console.log('_getHostName(url)', _getHostName(url));
          switch (type) {
            case 1:
              if (_getHostName(url) == 'play.google.com') {
                return true;
              } else {
                return false;
              }
              break;
            case 2:
              if (_getHostName(url) == '9apps.com') {
                return true;
              } else {
                return false;
              }
              break;
            case 3:
              if (_getHostName(url) == 'amazon.com') {
                return true;
              } else {
                return false;
              }
          }
        }
        return undefined;
      }

      function _isPlayStoreValid() {
        if (ctrl.gameProfile.platforms[0].url === undefined || ctrl.gameProfile.platforms[0].url === "" || checkPlatformUrl(1, ctrl.gameProfile.platforms[0].url)) {
          return true;
        }
        return false;
      }

      function _is9AppsValid() {
        if (ctrl.gameProfile.platforms[2].url === undefined || ctrl.gameProfile.platforms[2].url === "" || checkPlatformUrl(2, ctrl.gameProfile.platforms[2].url)) {
          return true;
        }
        return false;
      }

      function _isAmazonValid() {
        if (ctrl.gameProfile.platforms[1].url === undefined || ctrl.gameProfile.platforms[1].url === "" || checkPlatformUrl(3, ctrl.gameProfile.platforms[1].url)) {
          return true;
        }
        return false;
      }

      function isGameDetailsValid() {
        if (_isPlayStoreValid() && _is9AppsValid() && _isAmazonValid()) {
          return true;
        }
        return false;
      }

      function saveGameDetails() {
        ctrl.gameDetailsError = '';
        var ageGroup = [];

        ageGroup.push({
          min_age: 0,
          max_age: 15,
          value: ctrl.gameDetails.ageGroup.agegroup0 || 0
        });
        ageGroup.push({
          min_age: 16,
          max_age: 25,
          value: ctrl.gameDetails.ageGroup.agegroup16 || 0
        });
        ageGroup.push({
          min_age: 26,
          max_age: 40,
          value: ctrl.gameDetails.ageGroup.agegroup26 || 0
        });
        ageGroup.push({
          min_age: 41,
          max_age: 60,
          value: ctrl.gameDetails.ageGroup.agegroup41 || 0
        });
        ageGroup.push({
          min_age: 60,
          max_age: 90,
          value: ctrl.gameDetails.ageGroup.agegroup60 || 0
        });

        var resource = Gamedetails.save({ gameid: ctrl.gameId }, {
          age: ageGroup,
          gender: ctrl.gameDetails.demography
        }, function(data1) {
          ctrl.gameDetails.demography = data1.gender;
          ctrl.gameDetails.ageGroup = {};
          for (var i = 0; i < ((data1.age) ? data1.age.length : 0); i++) {
            var min_age = data1.age[i].min_age;
            ctrl.gameDetails.ageGroup['agegroup' + min_age] = data1.age[i].value || 0;
          }
          ctrl.gameDetailsCopy = angular.copy(ctrl.gameDetails);
          toastr.success('Game Demographics Saved');
        }, function(error) {
          ctrl.gameDetailsError = error;
        });

        return resource.$promise;
      }

      function saveGame() {
        ctrl.gameProfileError = '';
        var params = {};
        if (ctrl.gameId != 'new') {
          params = {
            id: $rootScope.currentGameId
          };
        }
        ctrl.gameProfile.group_owner = $rootScope.currentGroup.id;
        var platforms = [];
        var i = 1;
        ctrl.gameProfile.platforms.forEach(function(p) {
          p.id = i;
          platforms.push(p);
          i = i + 1;
        });

        ctrl.gameProfile.platforms = platforms;
        var resource = GameProfile.save(params, ctrl.gameProfile, function(data) {
            data.platforms = _initializeGamePlatforms(data.platforms);
            ctrl.gameProfile = data;
            toastr.success('Game Details Saved');
            ctrl.gameProfileCopy = angular.copy(ctrl.gameProfile);
            var results = [];
            $rootScope.gameProfiles.results.forEach(function(gp) {
              if (gp.id === $rootScope.currentGameId) {
                gp.title = $rootScope.currentGameProfile.title;
              }
              results.push(gp);
            });
            $rootScope.gameProfiles.results = results;
            if (ctrl.gameId === 'new') {
              //need to clear cache on saving new game since urls differes from that
              GameProfile.clearCache();
              $location.path("/" + data.id + "/wizard/1-a");
            }
          },
          function(error) {
            ctrl.gameProfileError = error
          });

        return resource.$promise
      };
    };

    function initStep2() {

      ctrl.getNativeImgLocation = getNativeImgLocation;
      ctrl.openImagesGallery = openImagesGallery;
      ctrl.createEmptyFloat = createEmptyFloat;
      ctrl.openAdspotmarkingDemo = openAdspotmarkingDemo;
      ctrl.adunitRemove = adunitRemove;
      ctrl.adunitEdit = adunitEdit;
      ctrl.realEstate = realEstate;
      ctrl.setOperation = setOperation;
      ctrl.removePlacements = removePlacements;
      ctrl.processPSD = processPSD;
      ctrl.uploadPSD = uploadPSD;
      ctrl.searchParams = $location.search();
      if (ctrl.searchParams.reatestate) {
        openRealEstateModal(parseInt(ctrl.searchParams.reatestate), ctrl.gameId)
      }

      function getNativeImgLocation() {
        if (ctrl.gameGenres.length > 0 && ctrl.gameProfile.genre && ctrl.gameProfile.genre <= 10) {
          return ($filter('filter')(ctrl.gameGenres, { id: ctrl.gameProfile.genre }))[0].name;
        }
        return 'default';
      }
      // Float ad unit

      function openImagesGallery(index, type) {
        var images = null;
        if (type == 1) {
          if (index < 4) {
            images = [{
              'src': 'native_units/' + ctrl.getNativeImgLocation() + '/1.jpg'
            }, {
              'src': 'native_units/' + ctrl.getNativeImgLocation() + '/2.jpg'
            }, {
              'src': 'native_units/' + ctrl.getNativeImgLocation() + '/3.jpg'
            }, {
              'src': 'native_units/' + ctrl.getNativeImgLocation() + '/4.jpg'
            }];

          } else {
            images = [{
              'src': 'native_1.jpeg'
            }, {
              'src': 'native_3.png'
            }, {
              'src': 'native_2.png'
            }, {
              'src': 'native_4.png'
            }];
            index = index - 4;
          }
        } else {
          images = [{
            'src': 'float_1.png'
          }, {
            'src': 'float_2.png'
          }, {
            'src': 'float_3.jpeg'
          }, {
            'src': 'float_4.jpeg'
          }, {
            'src': 'float_5.png'
          }, {
            'src': 'float_6.jpeg'
          }];

        }

        console.log(images);
        Lightbox.openModal(images, index);
      }



      function createEmptyFloat() {
        var modelData = {
          isLoading: true,
          isError: false,
          namespace: "Loading..."
        };
        ctrl.floatUnits.results.splice(0, 0, modelData);
        Unit.createFloatAd({
            gameId: ctrl.gameId,
            unit_type: 1
          },
          function(data) {
            modelData.isLoading = false;
            modelData.namespace = data.namespace;
            modelData.id = data.id;
            modelData.unit_type = data.unit_type;
            ctrl.isZeroFloatUnit = false;
            Unit.clearCache();
          });
      };

      function openAdspotmarkingDemo(type) {
        var modalInstance = $uibModal.open({
          animation: false,
          templateUrl: '/components/integrationwizard/adSpotmarkingDemoModal.html',
          controller: 'adSpotmarkingDemoCtrl',
          controllerAs: 'ctrl',
          size: 'lg',
          resolve: {
            adspotmarkingType: function() {
              return type;
            }
          }
        });
      };

      function adunitRemove(adUnit, adUnitType) {
        adUnit.serverError = '';
        Unit.removeUnit({
            gameId: ctrl.gameId,
            entity: adUnit.id,
            unit_type: adUnitType
          },
          function(data) {
            var index = 0;
            if (adUnitType === 0) {
              index = ctrl.nativeUnits.results.indexOf(adUnit);
              ctrl.nativeUnits.results.splice(index, 1);
            } else if (adUnitType === 1) {
              index = ctrl.floatUnits.results.indexOf(adUnit);
              ctrl.floatUnits.results.splice(index, 1);
            }
            Unit.clearCache();
          },
          function(error) {
            adUnit.serverError = error;
            ctrl.errorMsg = ctrl.errorMsg || [];
            ctrl.errorMsg.push(error.data);
          });
      };

      function adunitEdit(adunit) {
        //newAdunit.serverError = '';
        var modalInstance = $uibModal.open({
          animation: false,
          templateUrl: '/components/adunits/adunitmodal.html',
          controller: 'adUnitModalCtrl',
          controllerAs: 'ctrl',
          size: 'lg',
          backdrop: 'static',
          keyboard: false,
          resolve: {
            adunit: function() {
              return adunit;
            }
          }
        });
        modalInstance.result.then(function(newAdunit) {
          newAdunit.isBusy = true;
          adunit.serverError = '';
          Unit.save({
              gameId: ctrl.gameId
            }, newAdunit,
            function(data) {
              newAdunit.isBusy = false;
              adunit.description = data.description;
              Unit.clearCache();
            },
            function(error) {
              newAdunit.isBusy = false;
              adunit.serverError = error;
              ctrl.errorMsg = ctrl.errorMsg || [];
              ctrl.errorMsg.push(error.data);
            });
        }, function() {});
      };


      function openRealEstateModal(adunitId, gameId) {
        var modalInstance = $uibModal.open({
          animation: false,
          templateUrl: '/components/adunits/realesatemodal.html',
          controller: 'realEsateModalCtrl',
          controllerAs: 'ctrl',
          windowClass: 'realestate-modal-window',
          backdrop: 'static',
          keyboard: false,
          resolve: {
            adunitId: function() {
              return adunitId;
            },
            gameId: function() {
              return gameId;
            }
          }
        });
        modalInstance.result.then(function() {
          var search = $location.search();
          delete search.reatestate;
          $location.search({
            reatestate: null
          });
        }, function() {
          var search = $location.search();
          delete search.reatestate;
          $location.search(search);
          $location.search({
            reatestate: null
          });
        });
      }



      function realEstate(adUnit) {
        $location.search({
          reatestate: adUnit.id
        });
        ctrl.searchParams = $location.search();
        openRealEstateModal(adUnit.id, ctrl.gameId);
      };

      function removePlacements(unit) {
        unit.progressError = null;
        unit.progressPercentage = null;
        Unit.realEstateRemove({
            gameId: $rootScope.currentGameId,
            unitId: unit.id
          }, function(data) {},
          function(error) {
            adUnit.serverError = error;
          });
      };

      function setOperation(unit, operation) {
        unit.progressError = null;
        unit.progressPercentage = null;
        var newUnit = {
          unit_type: 0,
          id: unit.id,
          operation_type: operation
        };
        unit.isBusy = true;
        Unit.save({
            gameId: ctrl.gameId
          }, newUnit,
          function(data) {
            unit.isBusy = false;
            unit.operation_type = data.operation_type;
            Unit.clearCache();
          },
          function(error) {
            unit.isBusy = false;
            ctrl.isAlertBoxVisible = true;
            ctrl.alert.msg = error.data.title[0];
          });
      };

      function processPSD(unit) {
        unit.progressError = null;
        unit.progressPercentage = "processing";
        UnitCreation.processPSD({
            unitId: unit.id,
            psdUrl: unit.placement_psd,
            gameId: $rootScope.currentGameId
          },
          function(psdData) {

            unit.isBusy = false;
            unit.progressError = null;
            unit.progressPercentage = null;

            var index = ctrl.nativeUnits.results.indexOf(unit);
            ctrl.nativeUnits.results[index] = psdData.response.unit;
            console.log(index);

            console.log(psdData.response.unit);
            console.log(psdData);
          },
          function(psdError) {
            unit.serverError = psdError;
            console.error(psdError);
            unit.isBusy = false;
            unit.progressError = psdError.response || "error while processing";
            unit.progressPercentage = null;

          });
      }

      function uploadPSD(unit, validFile, invalidFile) {
        console.log(validFile);
        unit.progressPercentage = "uploading 1%";
        unit.progressError = null;
        if (validFile) {
          Unit.placementS3Policy({
            unitId: unit.id,
            gameId: $rootScope.currentGameId
          }, function(s3Data) {
            console.log(s3Data);

            unit.progressPercentage = "uploading 3%";
            unit.progressError = null;
            Upload.upload({
              url: 'http://' + s3Data.bucket_url,
              method: 'POST',
              headers: { "noauth": "1" },
              data: {
                key: s3Data.key,
                acl: s3Data.acl,
                AWSAccessKeyId: s3Data.aws_access_key,
                policy: s3Data.policy,
                signature: s3Data.signature,
                file: validFile,
                submit: "Upload"
              },
              ignoreLoadingBar: true
            }).then(function(data) {
              console.log(data);

              unit.placement_psd = "http://greedygamemedia.s3.amazonaws.com/" + s3Data.key;

              var newUnit = {
                unit_type: 0,
                id: unit.id,
                operation_type: 1,
                placement_psd: unit.placement_psd
              };

              console.log(newUnit);

              unit.isBusy = true;
              Unit.save({
                  gameId: ctrl.gameId
                }, newUnit,
                function(data) {
                  unit.operation_type = data.operation_type;
                  unit.placement_psd = data.placement_psd;
                  Unit.clearCache();
                  ctrl.processPSD(unit);
                },
                function(error) {
                  unit.serverError = error;
                  unit.isBusy = false;
                  unit.progressPercentage = null;
                  unit.progressError = "error while uploading";
                });

            }, function(error) {
              unit.serverError = error;
              console.error(error);
              unit.progressPercentage = null;
              unit.isBusy = false;
              unit.progressError = "error while uploading";
            }, function(evt) {
              var p = parseInt(100.0 * evt.loaded / evt.total);
              if (p > 3 && p < 100) {
                unit.progressPercentage = "uploading " + p + "%";
              }
              console.log(evt);
            });
          });
        }
      };
    };

    function initStep3() {
      ctrl.getGameEngine = getGameEngine;
      ctrl.isLoading = true;
      ctrl.androidPackage = getAndroidPackage(ctrl.gameProfile.platforms);
      ctrl.getTemplateUrl = SdkManual.getTemplateUrl(getengine(ctrl.gameProfile.engine));
      ctrl.androidXML = SdkManual.androidXML(ctrl.gameId, ctrl.nativeUnits, ctrl.floatUnits);
      ctrl.manifestChanges = SdkManual.manifestChanges(ctrl.androidPackage);
      ctrl.cocosInstall = SdkManual.cocosInstall();
      ctrl.initCall = SdkManual.initCall(getengine(ctrl.gameProfile.engine));
      ctrl.initListenerCall = SdkManual.initListenerCall(getengine(ctrl.gameProfile.engine));
      ctrl.stateRegisterCall = SdkManual.stateRegisterCall(getengine(ctrl.gameProfile.engine));
      ctrl.progressListner = SdkManual.progressListner(getengine(ctrl.gameProfile.engine));
      ctrl.progressRegisterCall = SdkManual.progressRegisterCall(getengine(ctrl.gameProfile.engine));
      if (ctrl.nativeUnits && ctrl.nativeUnits.results && ctrl.nativeUnits.results.length > 0) {
        ctrl.nativeUnitCall = SdkManual.nativeUnitCall(getengine(ctrl.gameProfile.engine), ctrl.nativeUnits.results[0].namespace, ctrl.nativeUnits.results[0].name);
      }
      if (ctrl.floatUnits && ctrl.floatUnits.results && ctrl.floatUnits.results.length > 0) {
        ctrl.floatUnitCall = SdkManual.floatUnitCall(getengine(ctrl.gameProfile.engine), ctrl.floatUnits.results[0].namespace);
        ctrl.extendedFloatUnitCall = SdkManual.extendedFloatUnitCall(getengine(ctrl.gameProfile.engine), ctrl.floatUnits.results[0].namespace);
      }
      ctrl.logCatCampaignAvailable = SdkManual.logCatCampaignAvailable(ctrl.gameId, ctrl.nativeUnits, ctrl.floatUnits, ctrl.androidPackage);


      function getGameEngine(gameEngine) {
        switch (gameEngine) {
          case 0:
            return 'unity-plugin';
          case 1:
            return 'cocos2dx-plugin';
          case 2:
            return 'android-native-plugin';
          default:
            return 'unity-plugin';
        }
      }
    };

    function initStep4() {

      ctrl.uploadApkFile = uploadApkFile;
      ctrl.addDevice = addDevice;
      ctrl.removeDevice = removeDevice;
      ctrl.fileNameInUrl = fileNameInUrl;
      ctrl.trigerDeviceTablebg = false;

      ctrl.gameApk = {};
      ctrl.stopBrandCreation = function() {
        ctrl.loaded = true;
      };

      ctrl.gameApks = GameProfile.apk({ id: ctrl.gameId },
        function(data) {},
        function(error) {
          ctrl.gameApksError = error;
        }
      );

      function uploadApkFile(file) {
        uploadAPKtoS3(file, function(data) {
          ctrl.gameApks.results.splice(0, 0, data);
          ctrl.trigerDeviceTablebg = true;
        });
      }

      ctrl.userDevice = {};
      $rootScope.$watch('currentGroup', function(nv) {
        if (nv && nv.id != null) {
          Device.query({}, function(data) {
            ctrl.userDevices = data;
            console.log(data);
          }, function(error) {
            ctrl.userDevicesError = error;
          });
        }
      });

      function addDevice(device) {
        device.group_owner = $rootScope.currentGroup.id;
        Device.create(device, function(data) {

          ctrl.userDevices.results.splice(0, 0, data);
          ctrl.userDevice = {};
        }, function(error) {
          device.serverError = error;
          ctrl.errorMsg = ctrl.errorMsg || [];
          ctrl.errorMsg.push(error.data);
          console.log(error.data);
        });
      };

      function removeDevice(device) {
        device.group = $rootScope.currentGroup.id;
        Device.delete(device, function(data) {

          var index = ctrl.userDevices.results.indexOf(device);
          if (index > -1) {
            ctrl.userDevices.results.splice(index, 1);
          }
        }, function(error) {
          device.serverError = error;
          ctrl.errorMsg = ctrl.errorMsg || [];
          ctrl.errorMsg.push(error.data);
          console.log(error.data);
        });
      };


      function fileNameInUrl(url) {
        var s = url.split("/");
        return s[s.length - 1];
      }
    };

    function initStep5() {
      ctrl.trigerDeviceTablebg = false;
      GameProfile.apk({ id: ctrl.gameId },
        function(data) {
          if (data.results.length > 0) {
            ctrl.gameStatus = $filter('orderBy')(data.results, '-created_epoch')[0];
          } else {
            ctrl.gameStatus = null;
          }
        },
        function(error) {
          ctrl.errorMsg = ctrl.errorMsg || [];
          ctrl.errorMsg.push(error.data);
          console.log(error.data);
        });


    };

    function closeAlert() {
      ctrl.errorMsg = [];
    }

    function click(step) {

      $location.path("/" + ctrl.gameId + "/wizard/" + step + "-a");
    };

    function getLeastStepToDo(step) {
      var leastStepToDo = 0;
      switch (step - 1) {
        case 5:
        case 4:
        case 3:
        case 2:
          if (ctrl.nativeUnits && ctrl.nativeUnits.results.length == 0 || ctrl.floatUnits && ctrl.floatUnits.results.length == 0) {
            leastStepToDo = 2;
          }
        case 1:
          if (ctrl.isGameProfileChanged() || !ctrl.gameProfile.id || ctrl.isGameDetailChanged()) {
            leastStepToDo = 1;
          }
      }
      console.log("leastStepToDo", leastStepToDo);
      return leastStepToDo;
    }

    var maxSubSteps = function(step) {
      switch (step) {
        case 1:
          return 1;
        case 2:
          return 4;
        case 3:
          return 5;
        case 4:
          return 3;
        case 5:
          return 1;
      }

    }

    function previousProcess(step) {
      if (ctrl.activeSubStep.charCodeAt(0) - 96 > 1) {
        ctrl.activeSubStep = String.fromCharCode(ctrl.activeSubStep.charCodeAt() - 1);
        $location.path("/" + ctrl.gameId + "/wizard/" + ctrl.activeStep + "-" + ctrl.activeSubStep);
      } else {
        step--;
        ctrl.activeSubStep = String.fromCharCode(96 + maxSubSteps(step));
        $location.path("/" + ctrl.gameId + "/wizard/" + step + "-" + ctrl.activeSubStep);
      }
    }

    function nextProcess(step) {
      var maxSteps = maxSubSteps(step);
      if (ctrl.activeSubStep.charCodeAt(0) - 96 < maxSteps) {
        ctrl.activeSubStep = String.fromCharCode(ctrl.activeSubStep.charCodeAt() + 1);
        $location.path("/" + ctrl.gameId + "/wizard/" + ctrl.activeStep + "-" + ctrl.activeSubStep);
      } else {
        ctrl.click(step + 1);
      }
    };

    function isNextProcessEnabled(step) {
      var maxSteps = maxSubSteps(step);
      if (ctrl.activeSubStep.charCodeAt(0) - 96 < maxSteps) {
        return true;
      } else {
        if (getLeastStepToDo(step + 1) > 0) {
          return false
        } else {
          return true;
        }
      }
    }





    var gup = function(name, url) {
      name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
      var regexS = "[\\?&]" + name + "=([^&#]*)";
      var regex = new RegExp(regexS);
      var results = regex.exec(url);
      return results == null ? null : results[1];
    };
    var getAndroidPackage = function(platforms) {
      var androidUrl = null;
      var androidPackage = "<your game package>";
      angular.forEach(platforms, function(p) {
        if (p.id === 1) {
          androidUrl = p.url;
          return;
        }
      });

      if (androidUrl) {
        androidPackage = gup('id', androidUrl);
      }
      return androidPackage;
    };

    var getengine = function(enginenumber) {
      if (enginenumber === 0) {
        return 'unity';
      } else if (enginenumber === 1) {
        return 'cocos';
      } else if (enginenumber === 2) {
        return 'android';
      }

    };

    ctrl.progressPercentage = 0;
    var uploadAPKtoS3 = function(apkFile, callback) {
      s3credentials.get({ gameId: $rootScope.currentGameId, fname: apkFile.name }, function(res) {
        res.key = res.key.replace(/ +/g, '-');
        ctrl.progressPercentage = 0;
        Upload.upload({
            url: 'http://' + res.bucket_url,
            method: 'POST',
            headers: { "noauth": "1" },
            data: {
              key: res.key,
              acl: res.acl,
              AWSAccessKeyId: res.aws_access_key,
              policy: res.policy,
              signature: res.signature,
              file: apkFile,
              submit: "Upload"
            },
            ignoreLoadingBar: true
          })
          .then(function(resp) {

            var apkurl = resp.config.url + '/' + resp.config.data.key;

            GameProfile.updateApkUrl({ id: ctrl.gameId }, { apk: apkurl },
              function(data) {

                callback(data);
                ctrl.progressPercentage = 0;
              },
              function(error) {
                ctrl.uploadApkError = error;
                console.log(file.$error);
                ctrl.errorMsg = ctrl.errorMsg || [];
                ctrl.errorMsg.push(file.$error);

              })

          }, function(error) {
            ctrl.uploadApkError = error;
            if (error.status === 403) {
              ctrl.errorMsg = ctrl.errorMsg || [];
              ctrl.errorMsg.push({ error: { 0: { 403: "Sorry! We have encounter problem while uploading, please re-try." } } });
            }
            console.log(error);
          }, function(evt) {
            var p = parseInt(100.0 * evt.loaded / evt.total);
            if (p < 100) {
              ctrl.progressPercentage = p;
            }
            console.log('progress: ' + ctrl.progressPercentage + '% ' + evt.config.data.file.name);
          });

      });
    }

    ctrl.newUnit = {};
    ctrl.newUnit.invalidfiles = [];

    // Native ad Unit
    ctrl.newNativeUnits = [];

    function uploadUnits(files, invalidfiles) {
      ctrl.upploadPngError = '';
      ctrl.newUnit.invalidfiles = invalidfiles;
      var indexKeep = {};
      if (files && files.length) {
        for (var i = 0; i < files.length; i++) {
          var file = files[i];
          if (!file.$error) {
            var modelData = {
              isLoading: true,
              isError: false,
              namespace: "Loading",
              name: file.name,
              file: file
            };
            indexKeep[i] = modelData;
            ctrl.newNativeUnits.splice(0, 0, modelData);
            Upload.upload({
              url: apiPrefix + ctrl.gameId + '/units',
              data: {
                creative: file,
                unit_type: 0
              }
            }).then(function(resp) {
              var index = files.indexOf(resp.config.data.creative);
              var obj = indexKeep[index];
              obj.isLoading = false;
              obj.isError = false;
              ctrl.nativeUnits.results.splice(0, 0, resp.data);
              Unit.clearCache();
            }, function(resp) {
              console.log(resp);
              var index = files.indexOf(resp.config.data.creative);
              var obj = indexKeep[index];
              obj.errorData = resp.data;
              obj.isLoading = false;
              obj.isError = true;
              obj.namespace = "Error!";
              ctrl.upploadPngError = resp;
            }, function(evt) {
              var index = files.indexOf(evt.config.data.creative);
              var obj = indexKeep[index];
              obj.namespace = "Loading...";
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              console.log(progressPercentage);
              obj.progressPercentage = progressPercentage;
            });
          } else {
            console.log(file.$error);
            ctrl.errorMsg = ctrl.errorMsg || [];
            ctrl.errorMsg.push(file.$error);
          }
        }
      } else {
        console.log("Input file variable in undefined");
      }
    };

  }
})();