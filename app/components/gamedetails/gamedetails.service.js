(function() {
  'use strict';
  angular.module('devPanelApp')
    .factory('Gamedetails', ['$resource', 'apiPrefix', Gamedetails]);

  function Gamedetails($resource, apiPrefix) {
    var resource = $resource(apiPrefix + 'games/:gameid/demographics', {
      gameid: '@gameid'
    });
    return resource;
  };
})();
