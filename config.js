var environments ={
  development: {
     apiPrefix: process.env.PUBLISHER_API_PREFIX || 'http://api-publisher.greedygame.com/',
     socketPrefix: process.env.PUBLISHER_SOCKET_PREFIX ||  'http://api-publisher.greedygame.com:8080/',
     socketPostbackPrefix: process.env.PUBLISHER_SOCKET_POSTBACK_PREFIX || 'http://service-publisher-1:8080/',
    dataApiPrefix:  process.env.PUBLISHER_DATA_API_PREFIX || 'http://api-data.greedygame.com/',
     version: process.env.PUBLISHER_VERSION ||  'production'
  },
  beta: {
    apiPrefix: 'http://api-publisher.greedylab.com/',
    dataApiPrefix: 'http://api-data.greedygame.com/',
    socketPrefix: 'http://api-publisher.greedylab.com:8080/',
    socketPostbackPrefix: 'http://service-publisher-beta:8080/',
    version: 'beta'
  },
  production: {
     apiPrefix: 'http://api-publisher.greedygame.com/',
     dataApiPrefix: 'http://api-data.greedygame.com/',
     socketPrefix: 'http://api-publisher.greedygame.com:8080/',
     socketPostbackPrefix: 'http://service-publisher-beta:8080/',
     version: 'production'
  }
}

module.exports = environments;